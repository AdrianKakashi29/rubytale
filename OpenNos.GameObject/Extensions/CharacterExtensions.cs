﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenNos.Domain;

namespace OpenNos.GameObject.Extensions
{
   public static class CharacterExtensions
    {
        public static string GetFamilyNameType(this Character e)
        {
            var thisRank = e.FamilyCharacter.Authority;

            return thisRank == FamilyAuthority.Member ? "918" :
                thisRank == FamilyAuthority.Familykeeper ? "917" :
                thisRank == FamilyAuthority.Familydeputy ? "916" :
                thisRank == FamilyAuthority.Head ? "915" : "-1 -";
        }

        public static string GetClassType(this Character e)
        {
            var thisClass = e.Class;

            return thisClass == ClassType.Adventurer ? "35" :
                thisClass == ClassType.Swordsman ? "36" :
                thisClass == ClassType.Archer ? "37" :
                thisClass == ClassType.Magician ? "38" :
                thisClass == ClassType.MartialArtist ? "39" : "0";
        }
    }
}
