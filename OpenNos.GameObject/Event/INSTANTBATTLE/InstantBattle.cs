﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event
{
    public static class InstantBattle
    {
        #region Methods

        public static void GenerateInstantBattle()
        {
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES"), 5), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES"), 5), 1));
            Thread.Sleep(4 * 60 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES"), 1), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES"), 1), 1));
            Thread.Sleep(30 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_SECONDS"), 30), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_SECONDS"), 30), 1));
            Thread.Sleep(20 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_SECONDS"), 10), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_SECONDS"), 10), 1));
            Thread.Sleep(10 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_STARTED"), 1));
            ServerManager.Instance.Sessions.Where(s => s.Character?.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance).ToList().ForEach(s => s.SendPacket($"qnaml 1 #guri^506 {Language.Instance.GetMessageFromKey("INSTANTBATTLE_QUESTION")}"));
            ServerManager.Instance.EventInWaiting = true;
            Thread.Sleep(30 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_STARTED"), 1));
            ServerManager.Instance.Sessions.Where(s => s.Character?.IsWaitingForEvent == false).ToList().ForEach(s => s.SendPacket("esf"));
            ServerManager.Instance.EventInWaiting = false;
            IEnumerable<ClientSession> sessions = ServerManager.Instance.Sessions.Where(s => s.Character?.IsWaitingForEvent == true && s.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance);
            List<Tuple<MapInstance, byte>> maps = new List<Tuple<MapInstance, byte>>();
            MapInstance map = null;
            int i = -1;
            int level = 0;
            byte instancelevel = 1;
            foreach (ClientSession s in sessions.OrderBy(s => s.Character?.Level))
            {
                i++;
                if (s.Character.Level > 97)
                {
                    i = 0;
                    instancelevel = 99;
                }
                else if (s.Character.Level > 79 && level <= 79)
                {
                    i = 0;
                    instancelevel = 80;
                }
                else if (s.Character.Level > 69 && level <= 69)
                {
                    i = 0;
                    instancelevel = 70;
                }
                else if (s.Character.Level > 59 && level <= 59)
                {
                    i = 0;
                    instancelevel = 60;
                }
                else if (s.Character.Level > 49 && level <= 49)
                {
                    i = 0;
                    instancelevel = 50;
                }
                else if (s.Character.Level > 39 && level <= 39)
                {
                    i = 0;
                    instancelevel = 40;
                }
                if (i % 50 == 0)
                {
                    map = ServerManager.GenerateMapInstance(2004, MapInstanceType.NormalInstance, new InstanceBag());
                    maps.Add(new Tuple<MapInstance, byte>(map, instancelevel));
                }
                if (map != null)
                {
                    ServerManager.Instance.TeleportOnRandomPlaceInMap(s, map.MapInstanceId);
                }

                level = s.Character.Level;
            }
            ServerManager.Instance.Sessions.Where(s => s.Character != null).ToList().ForEach(s => s.Character.IsWaitingForEvent = false);
            ServerManager.Instance.StartedEvents.Remove(EventType.INSTANTBATTLE);
            foreach (Tuple<MapInstance, byte> mapinstance in maps)
            {
                InstantBattleTask task = new InstantBattleTask();
                Observable.Timer(TimeSpan.FromMinutes(0)).Subscribe(X => InstantBattleTask.Run(mapinstance));
            }
        }

        #endregion

        #region Classes

        public class InstantBattleTask
        {
            #region Methods

            public static void Run(Tuple<MapInstance, byte> mapinstance)
            {
                long maxGold = ServerManager.Instance.Configuration.MaxGold;
                Thread.Sleep(10 * 1000);
                if (!mapinstance.Item1.Sessions.Skip(1 - 1).Any())
                {
                    mapinstance.Item1.Sessions.Where(s => s.Character != null).ToList().ForEach(s => {
                        s.Character.RemoveBuffByBCardTypeSubType(new List<KeyValuePair<byte, byte>>()
                        {
                            new KeyValuePair<byte, byte>((byte)BCardType.CardType.SpecialActions, (byte)AdditionalTypes.SpecialActions.Hide),
                            new KeyValuePair<byte, byte>((byte)BCardType.CardType.FalconSkill, (byte)AdditionalTypes.FalconSkill.Hide),
                            new KeyValuePair<byte, byte>((byte)BCardType.CardType.FalconSkill, (byte)AdditionalTypes.FalconSkill.Ambush)
                        });
                        ServerManager.Instance.ChangeMap(s.Character.CharacterId, s.Character.MapId, s.Character.MapX, s.Character.MapY);
                    });
                }
                Observable.Timer(TimeSpan.FromMinutes(12)).Subscribe(X =>
                {
                    for (int d = 0; d < 180; d++)
                    {
                        if (!mapinstance.Item1.Monsters.Any(s => s.CurrentHp > 0))
                        {
                            EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(0), new EventContainer(mapinstance.Item1, EventActionType.SPAWNPORTAL, new Portal { SourceX = 47, SourceY = 33, DestinationMapId = 1 }));
                            mapinstance.Item1.Broadcast(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_SUCCEEDED"), 0));
                            foreach (ClientSession cli in mapinstance.Item1.Sessions.Where(s => s.Character != null).ToList())
                            {
                                cli.Character.GenerateFamilyXp(cli.Character.Level * 10);
                                cli.Character.GetReputation(cli.Character.Level * 50);
                                cli.Character.Gold += cli.Character.Level * 25000;
                                cli.Character.Gold = cli.Character.Gold > maxGold ? maxGold : cli.Character.Gold;
                                cli.Character.SpAdditionPoint += cli.Character.Level * 100;

                                if (cli.Character.SpAdditionPoint > 1000000)
                                {
                                    cli.Character.SpAdditionPoint = 1000000;
                                }

                                cli.SendPacket(cli.Character.GenerateSpPoint());
                                cli.SendPacket(cli.Character.GenerateGold());
                                cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_MONEY"), cli.Character.Level * 25000), 10));
                                cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_REPUT"), cli.Character.Level * 50), 10));
                                if (cli.Character.Family != null)
                                {
                                    cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_FXP"), cli.Character.Level * 10), 10));
                                }
                                cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_SP_POINT"), cli.Character.Level * 100), 10));
                            }
                            break;
                        }
                        Thread.Sleep(1000);
                    }
                });

                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(15), new EventContainer(mapinstance.Item1, EventActionType.DISPOSEMAP, null));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(3), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES_REMAINING"), 12), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES_REMAINING"), 10), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(10), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES_REMAINING"), 5), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(11), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES_REMAINING"), 4), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(12), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES_REMAINING"), 3), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(13), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES_REMAINING"), 2), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(14), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MINUTES_REMAINING"), 1), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(14.5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_SECONDS_REMAINING"), 30), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(14.5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("INSTANTBATTLE_SECONDS_REMAINING"), 30), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(0), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MONSTERS_INCOMING"), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MONSTERS_HERE"), 0)));

                for (int wave = 0; wave < 4; wave++)
                {
                    EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(130 + (wave * 160)), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MONSTERS_WAVE"), 0)));
                    EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(160 + (wave * 160)), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MONSTERS_INCOMING"), 0)));
                    EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(170 + (wave * 160)), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANTBATTLE_MONSTERS_HERE"), 0)));
                    EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10 + (wave * 160)), new EventContainer(mapinstance.Item1, EventActionType.SPAWNMONSTERS, getInstantBattleMonster(mapinstance.Item1.Map, mapinstance.Item2, wave)));
                    EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(140 + (wave * 160)), new EventContainer(mapinstance.Item1, EventActionType.DROPITEMS, getInstantBattleDrop(mapinstance.Item1.Map, mapinstance.Item2, wave)));
                }
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(650), new EventContainer(mapinstance.Item1, EventActionType.SPAWNMONSTERS, getInstantBattleMonster(mapinstance.Item1.Map, mapinstance.Item2, 4)));
            }

            private static IEnumerable<Tuple<short, int, short, short>> generateDrop(Map map, short vnum, int amountofdrop, int amount)
            {
                List<Tuple<short, int, short, short>> dropParameters = new List<Tuple<short, int, short, short>>();
                for (int i = 0; i < amountofdrop; i++)
                {
                    MapCell cell = map.GetRandomPosition();
                    dropParameters.Add(new Tuple<short, int, short, short>(vnum, amount, cell.X, cell.Y));
                }
                return dropParameters;
            }

            private static List<Tuple<short, int, short, short>> getInstantBattleDrop(Map map, short instantbattletype, int wave)
            {
                List<Tuple<short, int, short, short>> dropParameters = new List<Tuple<short, int, short, short>>();
                switch (instantbattletype)
                {

                    case 1:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 20000));
                                dropParameters.AddRange(generateDrop(map, 1002, 10, 3));
                                dropParameters.AddRange(generateDrop(map, 1005, 10, 3));
                                break;

                            case 1:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 25000));
                                dropParameters.AddRange(generateDrop(map, 1002, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1005, 10, 5));
                                break;

                            case 2:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 30000));
                                dropParameters.AddRange(generateDrop(map, 1003, 10, 3));
                                dropParameters.AddRange(generateDrop(map, 1006, 10, 3));
                                break;

                            case 3:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 35000));
                                dropParameters.AddRange(generateDrop(map, 1003, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1006, 10, 5));
                                break;
                        }
                        break;

                    case 40:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 20000));
                                dropParameters.AddRange(generateDrop(map, 1008, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 180, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 181, 10, 1));
                                break;

                            case 1:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 30000));
                                dropParameters.AddRange(generateDrop(map, 1008, 10, 10));
                                dropParameters.AddRange(generateDrop(map, 180, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 181, 10, 1));
                                break;

                            case 2:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 35000));
                                dropParameters.AddRange(generateDrop(map, 1009, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1246, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 1247, 10, 1));
                                break;

                            case 3:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 40000));
                                dropParameters.AddRange(generateDrop(map, 1009, 10, 10));
                                dropParameters.AddRange(generateDrop(map, 1248, 10, 1));
                                break;
                        }
                        break;

                    case 50:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 35000));
                                dropParameters.AddRange(generateDrop(map, 1003, 15, 5));
                                dropParameters.AddRange(generateDrop(map, 1246, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 4, 2));
                                dropParameters.AddRange(generateDrop(map, 1030, 4, 2));
                                break;

                            case 1:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 40000));
                                dropParameters.AddRange(generateDrop(map, 1006, 15, 5));
                                dropParameters.AddRange(generateDrop(map, 1247, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 6, 2));
                                dropParameters.AddRange(generateDrop(map, 1030, 6, 2));
                                break;

                            case 2:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 45000));
                                dropParameters.AddRange(generateDrop(map, 1009, 15, 5));
                                dropParameters.AddRange(generateDrop(map, 1248, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 8, 2));
                                dropParameters.AddRange(generateDrop(map, 1030, 8, 2));
                                break;

                            case 3:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 50000));
                                dropParameters.AddRange(generateDrop(map, 1010, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1249, 5, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 10, 2));
                                dropParameters.AddRange(generateDrop(map, 1030, 10, 2));
                                break;
                        }

                        break;

                    case 60:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 45000));
                                dropParameters.AddRange(generateDrop(map, 1010, 15, 3));
                                dropParameters.AddRange(generateDrop(map, 1246, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 4, 4));
                                dropParameters.AddRange(generateDrop(map, 1030, 4, 4));
                                break;

                            case 1:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 50000));
                                dropParameters.AddRange(generateDrop(map, 1010, 15, 3));
                                dropParameters.AddRange(generateDrop(map, 1247, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 6, 4));
                                dropParameters.AddRange(generateDrop(map, 1030, 6, 4));
                                break;

                            case 2:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 55000));
                                dropParameters.AddRange(generateDrop(map, 1010, 15, 3));
                                dropParameters.AddRange(generateDrop(map, 1248, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 8, 4));
                                dropParameters.AddRange(generateDrop(map, 1030, 8, 4));
                                break;

                            case 3:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 60000));
                                dropParameters.AddRange(generateDrop(map, 1011, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1249, 5, 1));
                                dropParameters.AddRange(generateDrop(map, 1030, 10, 4));
                                dropParameters.AddRange(generateDrop(map, 2282, 10, 4));
                                break;
                        }
                        break;

                    case 70:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 55000));
                                dropParameters.AddRange(generateDrop(map, 1010, 15, 3));
                                dropParameters.AddRange(generateDrop(map, 1246, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 4, 6));
                                dropParameters.AddRange(generateDrop(map, 1030, 4, 6));
                                break;

                            case 1:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 60000));
                                dropParameters.AddRange(generateDrop(map, 1010, 15, 3));
                                dropParameters.AddRange(generateDrop(map, 1247, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 6, 6));
                                dropParameters.AddRange(generateDrop(map, 1030, 6, 6));
                                break;

                            case 2:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 65000));
                                dropParameters.AddRange(generateDrop(map, 1010, 15, 3));
                                dropParameters.AddRange(generateDrop(map, 1248, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 8, 6));
                                dropParameters.AddRange(generateDrop(map, 1030, 8, 6));
                                break;

                            case 3:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 70000));
                                dropParameters.AddRange(generateDrop(map, 1011, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1249, 10, 1));
                                dropParameters.AddRange(generateDrop(map, 2282, 10, 6));
                                dropParameters.AddRange(generateDrop(map, 1030, 10, 6));
                                break;
                        }
                        break;

                    case 80:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 100000));
                                dropParameters.AddRange(generateDrop(map, 1011, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1246, 10, 2));
                                dropParameters.AddRange(generateDrop(map, 2282, 10, 8));
                                dropParameters.AddRange(generateDrop(map, 1030, 10, 8));
                                break;

                            case 1:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 120000));
                                dropParameters.AddRange(generateDrop(map, 1011, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1247, 10, 2));
                                dropParameters.AddRange(generateDrop(map, 2282, 12, 8));
                                dropParameters.AddRange(generateDrop(map, 1030, 12, 8));
                                break;

                            case 2:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 140000));
                                dropParameters.AddRange(generateDrop(map, 1011, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1248, 10, 2));
                                dropParameters.AddRange(generateDrop(map, 2282, 14, 8));
                                dropParameters.AddRange(generateDrop(map, 1030, 14, 8));
                                break;

                            case 3:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 160000));
                                dropParameters.AddRange(generateDrop(map, 1011, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 1249, 10, 2));
                                dropParameters.AddRange(generateDrop(map, 2282, 16, 8));
                                dropParameters.AddRange(generateDrop(map, 1030, 16, 8));
                                break;
                        }
                        break;

                    case 99:
                        switch (wave)
                        {
                            case 0:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 160000));
                                dropParameters.AddRange(generateDrop(map, 1011, 20, 10));
                                dropParameters.AddRange(generateDrop(map, 2282, 10, 10));
                                dropParameters.AddRange(generateDrop(map, 1030, 10, 10));
                                dropParameters.AddRange(generateDrop(map, 1218, 5, 1));
                                break;

                            case 1:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 180000));
                                dropParameters.AddRange(generateDrop(map, 1242, 20, 5));
                                dropParameters.AddRange(generateDrop(map, 2282, 15, 10));
                                dropParameters.AddRange(generateDrop(map, 1030, 15, 10));
                                dropParameters.AddRange(generateDrop(map, 1243, 15, 10));
                                dropParameters.AddRange(generateDrop(map, 5369, 5, 1));
                                break;

                            case 2:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 200000));
                                dropParameters.AddRange(generateDrop(map, 1243, 20, 5));
                                dropParameters.AddRange(generateDrop(map, 2282, 20, 10));
                                dropParameters.AddRange(generateDrop(map, 1030, 20, 10));
                                dropParameters.AddRange(generateDrop(map, 1242, 15, 10));
                                dropParameters.AddRange(generateDrop(map, 1363, 5, 1));
                                break;

                            case 3:
                                dropParameters.AddRange(generateDrop(map, 1046, 10, 250000));
                                dropParameters.AddRange(generateDrop(map, 1244, 10, 5));
                                dropParameters.AddRange(generateDrop(map, 2282, 25, 10));
                                dropParameters.AddRange(generateDrop(map, 1030, 25, 10));
                                dropParameters.AddRange(generateDrop(map, 1244, 15, 10));
                                dropParameters.AddRange(generateDrop(map, 1364, 5, 1));
                                break;
                        }
                        break;

                }
                return dropParameters;
            }

            private static List<MonsterToSummon> getInstantBattleMonster(Map map, short instantbattletype, int wave)
            {
                List<MonsterToSummon> SummonParameters = new List<MonsterToSummon>();

                switch (instantbattletype)
                {

                    case 1:
                        switch (wave)
                        {
                            case 0:
                                SummonParameters.AddRange(map.GenerateMonsters(136, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(111, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(107, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(58, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1, 16, true, new List<EventContainer>()));
                                break;

                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(194, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(114, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(99, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(39, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(2, 16, true, new List<EventContainer>()));
                                break;

                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(140, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(100, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(81, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(12, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(4, 16, true, new List<EventContainer>()));
                                break;

                            case 3:
                                SummonParameters.AddRange(map.GenerateMonsters(115, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(112, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(110, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(14, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(5, 16, true, new List<EventContainer>()));
                                break;

                            case 4:
                                SummonParameters.AddRange(map.GenerateMonsters(167, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(137, 10, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(22, 15, false, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(17, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(723, 1, true, new List<EventContainer>()));
                                break;

                        }
                        break;

                    case 40:
                        switch (wave)
                        {
                            case 0:
                                SummonParameters.AddRange(map.GenerateMonsters(120, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(151, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(149, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(139, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(73, 16, true, new List<EventContainer>()));
                                break;

                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(152, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(147, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(104, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(62, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(8, 16, true, new List<EventContainer>()));
                                break;

                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(153, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(132, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(86, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(76, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(68, 16, true, new List<EventContainer>()));
                                break;

                            case 3:
                                SummonParameters.AddRange(map.GenerateMonsters(134, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(91, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(133, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(70, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(89, 16, true, new List<EventContainer>()));
                                break;

                            case 4:
                                SummonParameters.AddRange(map.GenerateMonsters(154, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(200, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(77, 8, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(217, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(979, 1, true, new List<EventContainer>()));
                                break;
                        }
                        break;

                    case 50:
                        switch (wave)
                        {
                            case 0:
                                SummonParameters.AddRange(map.GenerateMonsters(134, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(91, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(89, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(77, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(71, 16, true, new List<EventContainer>()));
                                break;

                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(217, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(200, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(154, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(92, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(79, 16, true, new List<EventContainer>()));
                                break;

                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(235, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(226, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(214, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(204, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(201, 15, true, new List<EventContainer>()));
                                break;

                            case 3:
                                SummonParameters.AddRange(map.GenerateMonsters(249, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(236, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(227, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(218, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(202, 15, true, new List<EventContainer>()));
                                break;

                            case 4:
                                SummonParameters.AddRange(map.GenerateMonsters(583, 1, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(400, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(255, 8, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(253, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(251, 10, true, new List<EventContainer>()));
                                break;
                        }
                        break;

                    case 60:
                        switch (wave)
                        {
                            case 0:
                                SummonParameters.AddRange(map.GenerateMonsters(242, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(234, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(215, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(207, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(202, 13, true, new List<EventContainer>()));
                                break;

                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(402, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(253, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(237, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(216, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(205, 13, true, new List<EventContainer>()));
                                break;

                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(402, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(243, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(228, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(255, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(205, 13, true, new List<EventContainer>()));
                                break;

                            case 3:
                                SummonParameters.AddRange(map.GenerateMonsters(268, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(255, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(254, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(174, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(172, 13, true, new List<EventContainer>()));
                                break;

                            case 4:
                                SummonParameters.AddRange(map.GenerateMonsters(407, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(272, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(261, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(256, 12, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(725, 1, true, new List<EventContainer>()));
                                break;
                        }
                        break;

                    case 70:
                        switch (wave)
                        {
                            case 0:
                                SummonParameters.AddRange(map.GenerateMonsters(402, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(253, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(237, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(216, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(205, 15, true, new List<EventContainer>()));
                                break;

                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(402, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(243, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(228, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(225, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(205, 15, true, new List<EventContainer>()));
                                break;

                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(255, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(254, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(251, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(174, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(172, 15, true, new List<EventContainer>()));
                                break;

                            case 3:
                                SummonParameters.AddRange(map.GenerateMonsters(407, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(272, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(261, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(257, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(256, 15, true, new List<EventContainer>()));
                                break;

                            case 4:
                                SummonParameters.AddRange(map.GenerateMonsters(444, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(439, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(275, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(274, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(273, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(748, 1, true, new List<EventContainer>()));
                                break;
                        }
                        break;

                    case 80:
                        switch (wave)
                        {
                            case 0:
                                SummonParameters.AddRange(map.GenerateMonsters(1000, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1001, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1002, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1003, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1004, 15, true, new List<EventContainer>()));
                                break;

                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(1104, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1105, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1106, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1107, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1108, 15, true, new List<EventContainer>()));
                                break;

                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(1123, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1122, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1121, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1119, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1125, 15, true, new List<EventContainer>()));
                                break;

                            case 3:
                                SummonParameters.AddRange(map.GenerateMonsters(1154, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1155, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1146, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1150, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1148, 15, true, new List<EventContainer>()));
                                break;

                            case 4:
                                SummonParameters.AddRange(map.GenerateMonsters(1176, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1177, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1178, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1035, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1034, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1905, 1, true, new List<EventContainer>()));
                                break;
                        }
                        break;

                    case 99:
                        switch (wave)
                        {
                            case 0:
                                SummonParameters.AddRange(map.GenerateMonsters(1042, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1045, 15, false, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1918, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1909, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1043, 16, true, new List<EventContainer>()));
                                break;

                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(1043, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1917, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1053, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1923, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1048, 16, true, new List<EventContainer>()));
                                break;

                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(1911, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1053, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1049, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1910, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1051, 16, true, new List<EventContainer>()));
                                break;

                            case 3:
                                SummonParameters.AddRange(map.GenerateMonsters(1051, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1053, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1042, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1045, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1048, 15, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1049, 16, true, new List<EventContainer>()));
                                break;

                            case 4:
                                SummonParameters.AddRange(map.GenerateMonsters(1047, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1903, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1051, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1910, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1923, 13, true, new List<EventContainer>()));
                                SummonParameters.AddRange(map.GenerateMonsters(1099, 1, true, new List<EventContainer>()));
                                break;

                        }
                        break;
                }
                return SummonParameters;
            }

            #endregion
        }

        #endregion
    }
}