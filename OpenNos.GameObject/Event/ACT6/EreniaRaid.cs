﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event
{
    public static class EreniaRaid
    {
        #region Methods

        public static void GenerateRaid()
        {
            MapInstance Hell4Gate = ServerManager.GetMapInstance(ServerManager.GetBaseMapInstanceIdByMapId(236));
            Hell4Gate.CreatePortal(new Portal
            {
                Type = (byte)PortalType.Raid,
                SourceMapId = 236,
                SourceX = 130,
                SourceY = 117
            });

        }

        #endregion
    }

    public class Act6Thread
    {
        #region Members

        private readonly List<long> _wonFamilies = new List<long>();

        private short _bossMapId = 136;

        private bool _bossMove;

        private short _bossVNum = 563;

        private short _bossX = 55;

        private short _bossY = 11;

        private short _destPortalX = 55;

        private short _destPortalY = 80;

        private byte _faction;

        private short _mapId = 135;

        private MapInstanceType _raidType;

        private short _sourcePortalX = 146;

        private short _sourcePortalY = 43;

        #endregion

        #region Methods
    }
}
#endregion