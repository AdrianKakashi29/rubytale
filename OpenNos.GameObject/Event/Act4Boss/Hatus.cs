﻿using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenNos.GameObject.Event.Act4Boss
{
    class Hatus
    {
        #region Properties

        public static int AngelDamage { get; set; }

        public static MapInstance HatusMapInstance { get; set; }

        public static int DemonDamage { get; set; }

        public static bool IsLocked { get; set; }

        public static bool IsRunning { get; set; }

        public static int RemainingTime { get; set; }

        public static MapInstance HatusStartMap { get; set; }

        #endregion

        #region Methods

        public static void Run()
        {
            HatusBossRaidThread raidThread = new HatusBossRaidThread();
            Observable.Timer(TimeSpan.FromMinutes(0)).Subscribe(X => raidThread.Run());
        }

        #endregion

        public class HatusBossRaidThread
        {
            #region Methods
            public void Run()
            {
                Hatus.RemainingTime = 3600;
                const int interval = 3;

                Hatus.HatusMapInstance = ServerManager.GenerateMapInstance(138, MapInstanceType.HatusBoss, new InstanceBag());
                Hatus.HatusStartMap = ServerManager.GetMapInstance(ServerManager.GetBaseMapInstanceIdByMapId(134));

                Hatus.HatusMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 138,
                    SourceX = 35,
                    SourceY = 60,
                    DestinationMapId = 134,
                    DestinationX = 143,
                    DestinationY = 96,
                    Type = 20
                });

                Hatus.HatusStartMap.CreatePortal(new Portal
                {
                    SourceMapId = 134,
                    SourceX = 143,
                    SourceY = 96,
                    DestinationMapInstanceId = Hatus.HatusMapInstance.MapInstanceId,
                    DestinationX = 35,
                    DestinationY = 60,
                    Type = 20
                });

                List<EventContainer> onDeathEvents = new List<EventContainer>
                {
                    new EventContainer(Hatus.HatusMapInstance, EventActionType.SCRIPTEND, (byte)1)
                };

                MapMonster mapMonster = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 40,
                    MapX = 36,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster);

                MapMonster mapMonster1 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 47,
                    MapX = 33,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster1.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster1);

                MapMonster mapMonster2 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 53,
                    MapX = 26,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster2.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster2);

                MapMonster mapMonster3 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 48,
                    MapX = 22,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster3.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster3);

                MapMonster mapMonster4 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 39,
                    MapX = 26,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster4.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster4);

                MapMonster mapMonster5 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 31,
                    MapX = 25,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster5.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster5);

                MapMonster mapMonster6 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 24,
                    MapX = 20,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster6.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster6);

                MapMonster mapMonster7 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 19,
                    MapX = 41,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster7.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster7);

                MapMonster mapMonster8 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 27,
                    MapX = 45,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster8.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster8);

                MapMonster mapMonster9 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 36,
                    MapX = 47,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster9.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster9);

                MapMonster mapMonster10 = new MapMonster
                {
                    MonsterVNum = 575,
                    MapY = 47,
                    MapX = 44,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster10.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(mapMonster10);

                MapMonster BossMonster = new MapMonster
                {
                    MonsterVNum = 577,
                    MapY = 18,
                    MapX = 36,
                    MapId = Hatus.HatusMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = false,
                    MapMonsterId = Hatus.HatusMapInstance.GetNextMonsterId(),
                    ShouldRespawn = false
                };
                BossMonster.Initialize(Hatus.HatusMapInstance);
                Hatus.HatusMapInstance.AddMonster(BossMonster);

                MapMonster Boss = Hatus.HatusMapInstance.Monsters.Find(s => s.Monster.NpcMonsterVNum == 577);

                if (Boss != null)
                {
                    Boss.BattleEntity.OnDeathEvents = onDeathEvents;
                    Boss.IsBoss = true;
                }

                ServerManager.Shout(Language.Instance.GetMessageFromKey("HATUS_OPEN"), true);

                RefreshRaid(Hatus.RemainingTime);

                ServerManager.Instance.HatusBossStart = DateTime.Now;

                while (Hatus.RemainingTime > 0)
                {
                    Hatus.RemainingTime -= interval;
                    Thread.Sleep(interval * 1000);
                    RefreshRaid(Hatus.RemainingTime);
                }

                EndRaid();
            }
            private void EndRaid()
            {
                ServerManager.Shout(Language.Instance.GetMessageFromKey("HATUS_END"), true);

                foreach (Portal p in Hatus.HatusStartMap.Portals.Where(s => s.DestinationMapInstanceId == Hatus.HatusStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Hatus.HatusStartMap.Broadcast(p.GenerateGp());
                    Hatus.HatusStartMap.Portals.Remove(p);
                }
                foreach (ClientSession sess in Hatus.HatusStartMap.Sessions.ToList())
                {
                    ServerManager.Instance.ChangeMapInstance(sess.Character.CharacterId, Hatus.HatusStartMap.MapInstanceId, sess.Character.MapX, sess.Character.MapY);
                    Thread.Sleep(100);
                }
                EventHelper.Instance.RunEventAsync(new EventContainer(Hatus.HatusStartMap, EventActionType.DISPOSEMAP, null));
                Hatus.IsRunning = false;
                Hatus.AngelDamage = 0;
                Hatus.DemonDamage = 0;
                ServerManager.Instance.StartedEvents.Remove(EventType.HatusBoss);
            }

            private void LockRaid()
            {
                foreach (Portal p in Hatus.HatusStartMap.Portals.Where(s => s.DestinationMapInstanceId == Hatus.HatusStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Hatus.HatusStartMap.Broadcast(p.GenerateGp());
                    p.IsDisabled = false;
                    p.Type = (byte)PortalType.Closed;
                    Hatus.HatusStartMap.Broadcast(p.GenerateGp());
                }
                ServerManager.Shout(Language.Instance.GetMessageFromKey("HATUS_LOCKED"), true);
                Hatus.IsLocked = true;
            }

            private void RefreshRaid(int remaining)
            {
                int maxHp = ServerManager.GetNpcMonster(629).MaxHP;
                Hatus.HatusStartMap.Broadcast(UserInterfaceHelper.GenerateCHDM(maxHp, Hatus.AngelDamage, Hatus.DemonDamage, Hatus.RemainingTime));

                if (((maxHp / 10) * 8 < Hatus.AngelDamage + Hatus.DemonDamage) && !Hatus.IsLocked)
                {
                    LockRaid();
                }
            }
        }
    }
}
#endregion