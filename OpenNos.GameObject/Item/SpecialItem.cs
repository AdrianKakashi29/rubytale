﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Core.Extensions;
using OpenNos.DAL;
using OpenNos.Data;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OpenNos.GameObject.Extension;

namespace OpenNos.GameObject
{
    public class SpecialItem : Item
    {
        #region Instantiation

        public SpecialItem(ItemDTO item) : base(item)
        {
        }

        #endregion

        #region Methods

        // case vnum
        // Add here you Booster that are not allowed in Arena
        public bool IsNotAllowedInArena(int ItemVnum)
        {
            switch (ItemVnum)
            {
                case 9020:
                case 9021:
                case 9022:
                case 9074:
                case 1500:
                case 1002:
                case 1003:
                case 1004:
                case 1005:
                case 1006:
                case 1007:
                case 1008:
                case 1009:
                case 1010:
                case 1011:
                case 1087:

                    return true;
                default:
                    return false;
            }
        }

        public override void Use(ClientSession session, ref ItemInstance inv, byte Option = 0, string[] packetsplit = null)
        {
            short itemDesign = inv.Design;

            IList<int> GlaceItems = new List<int>() {
                1296,
                9074,
                5194,
                5916,
                5927,
                5928,
                5929,
                1246,
                1247,
                1248,
                9020,
                9022,
            };
            #region BoxItem

            List<BoxItemDTO> boxItemDTOs = ServerManager.Instance.BoxItems.Where(boxItem => boxItem.OriginalItemVNum == VNum && boxItem.OriginalItemDesign == itemDesign).ToList();

            if (boxItemDTOs.Any())
            {
                session.Character.Inventory.RemoveItemFromInventory(inv.Id);

                foreach (BoxItemDTO boxItemDTO in boxItemDTOs)
                {
                    if (ServerManager.RandomNumber() < boxItemDTO.Probability)
                    {
                        session.Character.GiftAdd(boxItemDTO.ItemGeneratedVNum, boxItemDTO.ItemGeneratedAmount, boxItemDTO.ItemGeneratedRare, boxItemDTO.ItemGeneratedUpgrade, boxItemDTO.ItemGeneratedDesign);
                    }
                }

                return;
            }

            #endregion
            
            if (ServerManager.Instance.ChannelId != 51 && inv.ItemVNum >= 12003 && inv.ItemVNum <= 12007)
            {
                return;
            }

            if (ServerManager.Instance.ChannelId == 51 && GlaceItems.Contains(inv.ItemVNum))
            {
                return;
            }

            //if (VNum == 15003) //Perfection Reset
            //{
                //if (session.Character.UseSp)
                //{
                   //ItemInstance specialistInstance = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                    //if(specialistInstance != null)
                    //{
                        //specialistInstance.SpDamage = 0;
                        //specialistInstance.SpDark = 0;
                        //specialistInstance.SpDefence = 0;
                        //specialistInstance.SpElement = 0;
                        //specialistInstance.SpFire = 0;
                        //specialistInstance.SpHP = 0;
                        //specialistInstance.SpLight = 0;
                        //specialistInstance.SpStoneUpgrade = 0;
                        //specialistInstance.SpWater = 0;
                        //session.Character.Inventory.RemoveItemAmount(15003, 1);
                        //session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("RESET_PERFECTION"), 10));
                    //}
                    //else
                    //{
                        //session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("TRANSFORMATION_NEEDED"), 10));
                    //}
                //}
            //}




            int CClassType = 0;
            if (VNum == 15005) // ChangeClass Swordman
            {
                CClassType = 1;
                if (session.Character.Level < 15 || session.Character.JobLevel < 20)
                {
                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("LOW_LVL"), 0));                    
                    return;
                }
                if (CClassType == (byte)session.Character.Class)
                {                    
                    return;
                }
                if (session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                {
                    session.Character.Inventory.AddNewToInventory((short)(4 + (CClassType * 14)), 1, InventoryType.Wear);
                    session.Character.Inventory.AddNewToInventory((short)(81 + (CClassType * 13)), 1, InventoryType.Wear);
                    session.Character.Inventory.AddNewToInventory(68, 1, InventoryType.Wear);
                    session.Character.ChangeClass((ClassType)CClassType, false);
                }
                else 
                {
                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                }
            }
            if (VNum == 15006)
            {
                CClassType = 2;
            }
            if (VNum == 15007)
            {
                CClassType = 3;
            }
            if (VNum == 15008)
            {
                CClassType = 4;
            }


            //Start ChangeClass
            if (CClassType != 0)
            {
                int error_check = 0; //so it does not dissapear if an error appears

                if (session.Character.Level < 15 || session.Character.JobLevel < 20)
                {

                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("LOW_LVL"), 0));
                    error_check = 1;
                    return;
                }
                if (session.Character.Level < 81 && CClassType == 4 || session.Character.JobLevel < 20 && CClassType == 4)
                {
                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("LOW_LVL") + " (81+)", 0));
                    error_check = 1;
                    return;
                }
                if (CClassType > 4 || CClassType < 1)
                {
                    error_check = 1;
                    return;
                }
                if (CClassType == (byte)session.Character.Class)
                {
                    error_check = 1;
                    return;
                }
                if (session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                {
                    session.Character.Inventory.AddNewToInventory((short)(4 + (CClassType * 14)), 1, InventoryType.Wear);
                    session.Character.Inventory.AddNewToInventory((short)(81 + (CClassType * 13)), 1, InventoryType.Wear);

                    switch (CClassType)
                    {
                        case 1:
                            session.Character.Inventory.AddNewToInventory(68, 1, InventoryType.Wear);
                            break;

                        case 2:
                            session.Character.Inventory.AddNewToInventory(78, 1, InventoryType.Wear);
                            break;

                        case 3:
                            session.Character.Inventory.AddNewToInventory(86, 1, InventoryType.Wear);
                            break;
                        case 4:
                            //TODO add items
                            session.Character.SendGift(session.Character.CharacterId, 5832, 1, 0, 0, 0, false);
                            break;

                    }

                    session.CurrentMapInstance?.Broadcast(session.Character.GenerateEq());
                    session.SendPacket(session.Character.GenerateEquipment());
                    session.Character.ChangeClass((ClassType)CClassType, false);
                }
                else
                {
                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                    error_check = 2;
                }

                if (error_check == 0)
                {
                    //session.SendPacket(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 6000));
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                }
                return;
            }
            //End Change Class

            // BackPack Expansion
            if (VNum == 9143)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.Expansion))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddYears(15),
                        StaticBonusType = StaticBonusType.Expansion
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            if (VNum == 5836)
            {
                session.SendPacket(session.Character.GenerateGB(0));
                session.SendPacket(session.Character.GenerateSMemo(6, "Account balance: " + session.Character.GoldBank + "000 Gold.\nGold in your possession:" + session.Character.Gold + ".\nThanks, for using Bank Cuarry services!"));
                
            }

            if (VNum == 5795)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.Expansion))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddDays(30),
                        StaticBonusType = StaticBonusType.Expansion
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            if (VNum == 5796)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.Expansion))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddDays(60),
                        StaticBonusType = StaticBonusType.Expansion
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            if (VNum == 5797)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.Expansion))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddYears(15),
                        StaticBonusType = StaticBonusType.Expansion
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            //AutoLoot Item
            
            if (VNum == 1570)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddDays(1),
                        StaticBonusType = StaticBonusType.AutoLoot
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            if (VNum == 1533)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddDays(7),
                        StaticBonusType = StaticBonusType.AutoLoot
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            if (VNum == 11026)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddDays(15),
                        StaticBonusType = StaticBonusType.AutoLoot
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            if (VNum == 11027)
            {
                if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.AutoLoot))
                {
                    session.Character.StaticBonusList.Add(new StaticBonusDTO
                    {
                        CharacterId = session.Character.CharacterId,
                        DateEnd = DateTime.Now.AddDays(30),
                        StaticBonusType = StaticBonusType.AutoLoot
                    });
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                }
            }

            if (session.Character.IsVehicled && Effect != 1000)
            {
                if (VNum == 5119 || VNum == 9071) // Speed Booster
                {
                    if (!session.Character.Buff.Any(s => s.Card.CardId == 336))
                    {
                        session.Character.VehicleItem.BCards.ForEach(s => s.ApplyBCards(session.Character.BattleEntity, session.Character.BattleEntity));
                        session.CurrentMapInstance.Broadcast($"eff 1 {session.Character.CharacterId} 885");
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                }
                else
                {
                    session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("CANT_DO_VEHICLED"), 10));
                }
                return;
            }

            if (VNum == 1431)
            {
                Mate equipedMate = session.Character.Mates?.SingleOrDefault(s => s.IsTeamMember && s.MateType == MateType.Partner);

                if (equipedMate != null)
                {
                    equipedMate.RemoveTeamMember();
                    session.Character.MapInstance.Broadcast(equipedMate.GenerateOut());
                }

                Mate mate = new Mate(session.Character, ServerManager.GetNpcMonster(317), 24, MateType.Partner);
                session.Character.Mates?.Add(mate);
                mate.RefreshStats();
                session.SendPacket($"ctl 2 {mate.PetId} 3");
                session.Character.MapInstance.Broadcast(mate.GenerateIn());
                session.SendPacket(UserInterfaceHelper.GeneratePClear());
                session.SendPackets(session.Character.GenerateScP());
                session.SendPackets(session.Character.GenerateScN());
                session.SendPacket(session.Character.GeneratePinit());
                session.SendPackets(session.Character.Mates.Where(s => s.IsTeamMember)
                    .OrderBy(s => s.MateType)
                    .Select(s => s.GeneratePst()));
                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
            }

            if (VNum == 1419)
            {
                Mate equipedMate = session.Character.Mates?.SingleOrDefault(s => s.IsTeamMember && s.MateType == MateType.Partner);

                if (equipedMate != null)
                {
                    equipedMate.RemoveTeamMember();
                    session.Character.MapInstance.Broadcast(equipedMate.GenerateOut());
                }

                Mate mate = new Mate(session.Character, ServerManager.GetNpcMonster(318), 31, MateType.Partner);
                session.Character.Mates?.Add(mate);
                mate.RefreshStats();
                session.SendPacket($"ctl 2 {mate.PetId} 3");
                session.Character.MapInstance.Broadcast(mate.GenerateIn());
                session.SendPacket(UserInterfaceHelper.GeneratePClear());
                session.SendPackets(session.Character.GenerateScP());
                session.SendPackets(session.Character.GenerateScN());
                session.SendPacket(session.Character.GeneratePinit());
                session.SendPackets(session.Character.Mates.Where(s => s.IsTeamMember)
                    .OrderBy(s => s.MateType)
                    .Select(s => s.GeneratePst()));
                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
            }

            if (VNum == 1431)
            {
                Mate equipedMate = session.Character.Mates?.SingleOrDefault(s => s.IsTeamMember && s.MateType == MateType.Partner);

                if (equipedMate != null)
                {
                    equipedMate.RemoveTeamMember();
                    session.Character.MapInstance.Broadcast(equipedMate.GenerateOut());
                }

                Mate mate = new Mate(session.Character, ServerManager.GetNpcMonster(319), 48, MateType.Partner);
                session.Character.Mates?.Add(mate);
                mate.RefreshStats();
                session.SendPacket($"ctl 2 {mate.PetId} 3");
                session.Character.MapInstance.Broadcast(mate.GenerateIn());
                session.SendPacket(UserInterfaceHelper.GeneratePClear());
                session.SendPackets(session.Character.GenerateScP());
                session.SendPackets(session.Character.GenerateScN());
                session.SendPacket(session.Character.GeneratePinit());
                session.SendPackets(session.Character.Mates.Where(s => s.IsTeamMember)
                    .OrderBy(s => s.MateType)
                    .Select(s => s.GeneratePst()));
                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
            }

            if (VNum == 5511)
            {
                session.Character.GeneralLogs.Where(s => s.LogType == "InstanceEntry" && (short.Parse(s.LogData) == 16 || short.Parse(s.LogData) == 17) && s.Timestamp.Date == DateTime.Today).ToList().ForEach(s =>
                {
                    s.LogType = "NulledInstanceEntry";
                    DAOFactory.GeneralLogDAO.InsertOrUpdate(ref s);
                });
                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                return;
            }

            if (session.CurrentMapInstance?.MapInstanceType != MapInstanceType.TalentArenaMapInstance
            && (VNum == 5936 || VNum == 5937 || VNum == 5938 || VNum == 5939 || VNum == 5940 || VNum == 5942 || VNum == 5943 || VNum == 5944 || VNum == 5945 || VNum == 5946))
            {
                return;
            }
            if (session.CurrentMapInstance?.MapInstanceType == MapInstanceType.TalentArenaMapInstance
            && VNum != 5936 && VNum != 5937 && VNum != 5938 && VNum != 5939 && VNum != 5940 && VNum != 5942 && VNum != 5943 && VNum != 5944 && VNum != 5945 && VNum != 5946)
            {
                return;
            }

            if (BCards.Count > 0 && Effect != 1000)
            {
                if (BCards.Any(s => s.Type == (byte)BCardType.CardType.Buff && s.SubType == 1 && new Buff((short)s.SecondData, session.Character.Level).Card.BCards.Any(newbuff => session.Character.Buff.GetAllItems().Any(b => b.Card.BCards.Any(buff =>
                    buff.CardId != newbuff.CardId
                 && ((buff.Type == 33 && buff.SubType == 5 && (newbuff.Type == 33 || newbuff.Type == 58)) || (newbuff.Type == 33 && newbuff.SubType == 5 && (buff.Type == 33 || buff.Type == 58))
                 || (buff.Type == 33 && (buff.SubType == 1 || buff.SubType == 3) && (newbuff.Type == 58 && (newbuff.SubType == 1))) || (buff.Type == 33 && (buff.SubType == 2 || buff.SubType == 4) && (newbuff.Type == 58 && (newbuff.SubType == 3)))
                 || (newbuff.Type == 33 && (newbuff.SubType == 1 || newbuff.SubType == 3) && (buff.Type == 58 && (buff.SubType == 1))) || (newbuff.Type == 33 && (newbuff.SubType == 2 || newbuff.SubType == 4) && (buff.Type == 58 && (buff.SubType == 3)))
                 || (buff.Type == 33 && newbuff.Type == 33 && buff.SubType == newbuff.SubType) || (buff.Type == 58 && newbuff.Type == 58 && buff.SubType == newbuff.SubType)))))))
                {
                    return;
                }
                BCards.ForEach(c => c.ApplyBCards(session.Character.BattleEntity, session.Character.BattleEntity));
                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                return;
            }

            switch (Effect)
            {
                // Seal Mini-Game
                case 1718:
                    switch (EffectValue)
                    {
                        case 1:// King Ratufu Mini Game
                               // Not Created for moment .
                            break;
                        case 2: // Sheep Mini Game
                            session.SendPacket($"say 1 {session.Character.CharacterId} 10 Registration starts in 5 seconds.");
                            //Need keys
                            EventHelper.GenerateEvent(EventType.SHEEPGAME);
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            break;
                        case 3: // Meteor Mini Game
                            session.SendPacket($"say 1 {session.Character.CharacterId} 10 Registration starts in 5 seconds.");
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            EventHelper.GenerateEvent(EventType.METEORITEGAME);
                            break;
                    }
                    break;

                case 930:
                    switch (EffectValue)
                    {
                        case 600:
                            if (session.Character.MapId > 0 && session.Character.Level > 29)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 2622)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.RAIDTIMESPACE.CubyTimeSpace.GenerateCubyTS(1826, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }                              
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 30 to do this!"), 0));
                            }
                            break;

                        case 607:
                            if (session.Character.MapId > 0 && session.Character.Level > 69)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 2622)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.RAIDTIMESPACE.SladeTimeSpace.GenerateSladeTS(1820, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 70 to do this!"), 0));
                            }
                            break;

                        case 604:
                            if (session.Character.MapId > 0 && session.Character.Level > 49)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 2622)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.RAIDTIMESPACE.CastraTimeSpace.GenerateCastraTS(1817, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 50 to do this!"), 0));
                            }
                            break;

                        case 605:
                            if (session.Character.MapId > 0 && session.Character.Level > 59)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 2622)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.RAIDTIMESPACE.SpiderTimeSpace.GenerateSpiderTS(1818, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 60 to do this!"), 0));
                            }
                            break;

                        case 601:
                            if (session.Character.MapId > 0 && session.Character.Level > 39)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 120, Y = 106 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 2622)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.RAIDTIMESPACE.GinsengTimespace.GenerateGinsengTS(1827, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 40 to do this!"), 0));
                            }
                            break;

                        case 507:
                            if (session.Character.MapId > 0 && session.Character.Level > 44)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.HochwertigTimespace.GenerateHTS(1807, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 45 to do this!"), 0));
                            }
                            break;

                        case 508:
                            if (session.Character.MapId > 0 && session.Character.Level > 44)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.KristallTimespace.GenerateKTS(1808, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 45 to do this!"), 0));
                            }
                            break;

                        case 509:
                            if (session.Character.MapId > 0 && session.Character.Level > 54)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.OrichalcumTimespace.GenerateOTS(1809, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 55 to do this!"), 0));
                            }
                            break;

                        case 510:
                            if (session.Character.MapId > 0 && session.Character.Level > 54)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.SeltenesBlattTimespace.GenerateSTS(1810, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 55 to do this!"), 0));
                            }
                            break;

                        case 511:
                            if (session.Character.MapId > 0 && session.Character.Level > 54)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.WurmTimespace.GenerateWTS(1811, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 55 to do this!"), 0));
                            }
                            break;

                        case 512:
                            if (session.Character.MapId > 0 && session.Character.Level > 54)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.MakreleTimespace.GenerateMTS(1812, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 55 to do this!"), 0));
                            }
                            break;

                        case 505:
                            //add delay
                            if (session.Character.MapId > 0 && session.Character.Level > 89)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.PTS.GeneratePTS(1805, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 90 to do this!"), 0));
                            }
                            break;

                        case 506:
                            //add delay
                            if (session.Character.MapId > 0 && session.Character.Level > 89)
                            {
                                int dist = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                int dist1 = Map.GetDistance(
                                    new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY },
                                    new MapCell { X = 80, Y = 116 });

                                if (dist < 6)
                                {
                                    session.SendPacket(session.Character.GenerateEff(820));
                                    session.SendPacket(session.Character.GenerateEff(821));
                                    session.SendPacket(session.Character.GenerateEff(6008));
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("PTS_NEAR"), 0));
                                }
                                if (dist < 3 && session.Character.MapId == 1)
                                {
                                    session.SendPacket(session.Character.GenerateEff(822));
                                    Event.FamilyXpTimeSpace.GenerateFXPPTS(1806, session);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                            else
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(("You need to be Atleast Lvl 90 to do this!"), 0));
                            }
                            break;
                    }
                    break;
                case 1717: // Sp SKins
                    ItemInstance sp = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                    if (session.Character.UseSp && sp != null && !session.Character.IsSeal)
                    {
                        List<long> spVNum = new List<long> { 902, 903, 913 };
                        if (!spVNum.Contains(sp.Item.VNum))
                        {
                            // cant use this item with this Sp weared
                            session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANT_USE_THAT_SP"), 0));
                            return;
                        }
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_SKIN_CHANGE")}");
                        }
                        else
                        {
                            sp.HaveSkin = true;
                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                            session.SendPacket(session.Character.GenerateStat());
                            session.SendPackets(session.Character.GenerateStatChar());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_SP"), 0));
                    }
                    break;

                // FastPerf
                case 1400:
                    {
                        try
                        {
                            ItemInstance specialistInstance1 = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                            if (specialistInstance1 != null)
                            {

                                short[] upsuccess = { 50, 40, 30, 20, 10 };

                                int[] goldprice = { 5000, 10000, 20000, 50000, 100000 };
                                byte[] stoneprice = { 1, 2, 3, 4, 5 };
                                short stonevnum;
                                byte upmode = 1;
                                short SpDamage = 0;
                                short SpDefence = 0;
                                short SpElement = 0;
                                short SpHP = 0;
                                short SpFire = 0;
                                short SpWater = 0;
                                short SpLight = 0;
                                short SpDark = 0;
                                short Fallimenti = 0;
                                short Successi = 0;

                                switch (specialistInstance1.Item.Morph)
                                {
                                    case 16:
                                    case 8:
                                    case 1:
                                        stonevnum = 2514;
                                        stonevnum = 2515;
                                        stonevnum = 2516;
                                        stonevnum = 2517;
                                        stonevnum = 2518;
                                        stonevnum = 2519;
                                        stonevnum = 2520;
                                        stonevnum = 2521;
                                        break;

                                    case 2:
                                    case 6:
                                    case 9:
                                    case 12:
                                    case 29:
                                        stonevnum = 2514;
                                        break;

                                    case 3:
                                    case 4:
                                    case 14:
                                    case 31:
                                        stonevnum = 2515;
                                        break;

                                    case 5:
                                    case 11:
                                    case 15:
                                    case 34:
                                        stonevnum = 2516;
                                        break;

                                    case 10:
                                    case 13:
                                    case 7:
                                    case 33:
                                        stonevnum = 2517;
                                        break;

                                    case 17:
                                    case 18:
                                    case 19:
                                        stonevnum = 2518;
                                        break;

                                    case 20:
                                    case 21:
                                    case 22:
                                        stonevnum = 2519;
                                        break;

                                    case 23:
                                    case 24:
                                    case 25:
                                        stonevnum = 2520;
                                        break;

                                    case 26:
                                    case 27:
                                    case 28:
                                        stonevnum = 2521;
                                        break;

                                    default:
                                        return;
                                }

                                while (session.Character.Inventory.CountItem(stonevnum) > 0)
                                {
                                    if (specialistInstance1.SpStoneUpgrade > 99)
                                    {
                                        break;
                                    }
                                    if (specialistInstance1.SpStoneUpgrade > 80)
                                    {
                                        upmode = 5;
                                    }
                                    else if (specialistInstance1.SpStoneUpgrade > 60)
                                    {
                                        upmode = 4;
                                    }
                                    else if (specialistInstance1.SpStoneUpgrade > 40)
                                    {
                                        upmode = 3;
                                    }
                                    else if (specialistInstance1.SpStoneUpgrade > 20)
                                    {
                                        upmode = 2;
                                    }

                                    if (session.Character.Gold < goldprice[upmode - 1])
                                    {
                                        break;
                                    }
                                    if (session.Character.Inventory.CountItem(stonevnum) < stoneprice[upmode - 1])
                                    {
                                        break;
                                    }
                                    int rnd = ServerManager.RandomNumber();
                                    if (rnd < upsuccess[upmode - 1])
                                    {
                                        byte Type = (byte)ServerManager.RandomNumber(0, 16), count = 1;
                                        if (upmode == 4)
                                        {
                                            count = 2;
                                        }
                                        if (upmode == 5)
                                        {
                                            count = (byte)ServerManager.RandomNumber(3, 6);
                                        }
                                        if (Type < 3)
                                        {
                                            specialistInstance1.SpDamage += count;
                                            SpDamage += count;
                                        }
                                        else if (Type < 6)
                                        {
                                            specialistInstance1.SpDefence += count;
                                            SpDefence += count;
                                        }
                                        else if (Type < 9)
                                        {
                                            specialistInstance1.SpElement += count;
                                            SpElement += count;
                                        }
                                        else if (Type < 12)
                                        {
                                            specialistInstance1.SpHP += count;
                                            SpHP += count;
                                        }
                                        else if (Type == 12)
                                        {
                                            specialistInstance1.SpFire += count;
                                            SpFire += count;
                                        }
                                        else if (Type == 13)
                                        {
                                            specialistInstance1.SpWater += count;
                                            SpWater += count;
                                        }
                                        else if (Type == 14)
                                        {
                                            specialistInstance1.SpLight += count;
                                            SpLight += count;
                                        }
                                        else if (Type == 15)
                                        {
                                            specialistInstance1.SpDark += count;
                                            SpDark += count;
                                        }
                                        specialistInstance1.SpStoneUpgrade++;
                                        Successi++;
                                    }
                                    else
                                    {
                                        Fallimenti++;
                                    }
                                    session.SendPacket(specialistInstance1.GenerateInventoryAdd());
                                    session.Character.Gold -= goldprice[upmode - 1];
                                    session.SendPacket(session.Character.GenerateGold());
                                    session.Character.Inventory.RemoveItemAmount(stonevnum, stoneprice[upmode - 1]);
                                }
                                if (Successi > 0 || Fallimenti > 0)
                                {
                                    session.CurrentMapInstance.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 3005), session.Character.MapX, session.Character.MapY);
                                    session.SendPacket(session.Character.GenerateSay("---------------Perfection Status---------------", 11));
                                    session.SendPacket(session.Character.GenerateSay("Success: " + Successi, 11));
                                    session.SendPacket(session.Character.GenerateSay("Fails: " + Fallimenti, 11));
                                    session.SendPacket(session.Character.GenerateSay("Attack: " + SpDamage, 11));
                                    session.SendPacket(session.Character.GenerateSay("Defense: " + SpDefence, 11));
                                    session.SendPacket(session.Character.GenerateSay("HP: " + SpHP, 11));
                                    session.SendPacket(session.Character.GenerateSay("Element " + SpElement, 11));
                                    session.SendPacket(session.Character.GenerateSay("Fire: " + SpFire, 11));
                                    session.SendPacket(session.Character.GenerateSay("Water: " + SpWater, 11));
                                    session.SendPacket(session.Character.GenerateSay("Light: " + SpLight, 11));
                                    session.SendPacket(session.Character.GenerateSay("Fark: " + SpDark, 11));
                                    session.SendPacket(session.Character.GenerateSay("-----------------------------------------------", 11));
                                }
                                else
                                {
                                    session.SendPacket(session.Character.GenerateSay("NOT_ENOUGH_ITEM", 10));
                                }
                            }
                            else
                            {
                                session.SendPacket(session.Character.GenerateSay("WEAR_SP", 10));
                            }
                        }
                        catch (Exception ex)
                        {
                            Logger.Error(ex);
                        }

                    }
                    break;




                // Honour Medals
                case 69:
                    session.Character.Reputation += ReputPrice;
                    session.SendPacket(session.Character.GenerateFd());
                    session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("REPUT_INCREASE"), ReputPrice), 11));
                    session.CurrentMapInstance?.Broadcast(session, session.Character.GenerateIn(InEffect: 1), ReceiverType.AllExceptMe);
                    session.CurrentMapInstance?.Broadcast(session, session.Character.GenerateGidx(), ReceiverType.AllExceptMe);
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                // TimeSpace Stones
                case 140:
                    if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                    {
                        if (ServerManager.Instance.TimeSpaces.FirstOrDefault(s => s.Id == EffectValue) is ScriptedInstance timeSpace)
                        {
                            session.Character.EnterInstance(timeSpace);
                        }
                    }
                    break;

                // SP Potions
                case 150:
                case 151:
                    {
                        if (session.Character.SpAdditionPoint >= 1000000)
                        {
                            session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SP_POINTS_FULL"), 0));
                            break;
                        }

                        session.Character.SpAdditionPoint += EffectValue;

                        if (session.Character.SpAdditionPoint > 1000000)
                        {
                            session.Character.SpAdditionPoint = 1000000;
                        }

                        session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("SP_POINTSADDED"), EffectValue), 0));
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateSpPoint());
                    }
                    break;

                // Specialist Medal
                case 204:
                    {
                        if (session.Character.SpPoint >= 10000
                            && session.Character.SpAdditionPoint >= 1000000)
                        {
                            session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("SP_POINTS_FULL"), 0));
                            break;
                        }

                        session.Character.SpPoint += EffectValue;

                        if (session.Character.SpPoint > 10000)
                        {
                            session.Character.SpPoint = 10000;
                        }

                        session.Character.SpAdditionPoint += EffectValue * 3;

                        if (session.Character.SpAdditionPoint > 1000000)
                        {
                            session.Character.SpAdditionPoint = 1000000;
                        }

                        session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("SP_POINTSADDEDBOTH"), EffectValue, EffectValue * 3), 0));
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateSpPoint());
                    }
                    break;

                // Raid Seals
                case 301:
                    ItemInstance raidSeal = session.Character.Inventory.LoadBySlotAndType(inv.Slot, InventoryType.Main);

                    if (raidSeal != null)
                    {
                        ScriptedInstance raid = ServerManager.Instance.Raids.FirstOrDefault(s => s.Id == raidSeal.Item.EffectValue)?.Copy();

                        if (raid != null)
                        {
                            if (ServerManager.Instance.ChannelId == 51 || session.CurrentMapInstance.MapInstanceType != MapInstanceType.BaseMapInstance)
                            {
                                return;
                            }

                            if (ServerManager.Instance.IsCharacterMemberOfGroup(session.Character.CharacterId))
                            {
                                session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("RAID_OPEN_GROUP"), 12));
                                return;
                            }

                            var entries = raid.DailyEntries - session.Character.GeneralLogs.CountLinq(s => s.LogType == "InstanceEntry" && short.Parse(s.LogData) == raid.Id && s.Timestamp.Date == DateTime.Today);

                            if (raid.DailyEntries > 0 && entries <= 0)
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("INSTANCE_NO_MORE_ENTRIES"), 0));
                                session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("INSTANCE_NO_MORE_ENTRIES"), 10));
                                return;
                            }

                            if (session.Character.Level > raid.LevelMaximum || session.Character.Level < raid.LevelMinimum)
                            {
                                session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("RAID_LEVEL_INCORRECT"), 10));
                                return;
                            }

                            Group group = new Group
                            {
                                GroupType = raid.IsGiantTeam ? GroupType.GiantTeam : raid.IsMediumTeam ? GroupType.MediumTeam : GroupType.BigTeam,
                                Raid = raid
                            };
                            if (group.JoinGroup(session))
                            {
                                ServerManager.Instance.AddGroup(group);
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("RAID_LEADER"), session.Character.Name), 0));
                                session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("RAID_LEADER"), session.Character.Name), 10));
                                session.SendPacket(session.Character.GenerateRaid(2));
                                session.SendPacket(session.Character.GenerateRaid(0));
                                session.SendPacket(session.Character.GenerateRaid(1));
                                session.SendPacket(group.GenerateRdlst());
                                session.Character.Inventory.RemoveItemFromInventory(raidSeal.Id);
                            }
                        }
                    }
                    break;

                // Partner Suits/Skins
                case 305:
                    Mate mate = session.Character.Mates.Find(s => s.MateTransportId == int.Parse(packetsplit[3]));
                    if (mate != null && EffectValue == mate.NpcMonsterVNum && mate.Skin == 0)
                    {
                        mate.Skin = Morph;
                        session.SendPacket(mate.GenerateCMode(mate.Skin));
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                //suction Funnel (Quest Item / QuestId = 1724)
                case 400:
                    if (session.Character == null || session.Character.Quests.All(q => q.QuestId != 1724))
                    {
                        break;
                    }
                    if (session.Character.Quests.FirstOrDefault(q => q.QuestId == 1724) is CharacterQuest kenkoQuest)
                    {
                        MapMonster kenko = session.CurrentMapInstance?.Monsters.FirstOrDefault(m => m.MapMonsterId == session.Character.LastNpcMonsterId && m.MonsterVNum > 144 && m.MonsterVNum < 154);
                        if (kenko == null || session.Character.Inventory.CountItem(1174) > 0)
                        {
                            break;
                        }
                        if (session.Character.LastFunnelUse.AddSeconds(30) <= DateTime.Now)
                        {
                            if (kenko.CurrentHp / kenko.MaxHp * 100 < 30)
                            {
                                if (ServerManager.RandomNumber() < 30)
                                {
                                    kenko.SetDeathStatement();
                                    session.Character.MapInstance.Broadcast(StaticPacketHelper.Out(UserType.Monster, kenko.MapMonsterId));
                                    session.Character.Inventory.AddNewToInventory(1174); // Kenko Bead
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("KENKO_CATCHED"), 0));
                                }
                                else { session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("QUEST_CATCH_FAIL"), 0)); }
                                session.Character.LastFunnelUse = DateTime.Now;
                            }
                            else { session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("HP_TOO_HIGH"), 0)); }
                        }
                    }
                    break;

                // Fairy Booster
                case 250:
                    if (!session.Character.Buff.ContainsKey(131))
                    {
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 131 });
                        session.CurrentMapInstance?.Broadcast(session.Character.GeneratePairy());
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), inv.Item.Name), 0));
                        session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 3014), session.Character.PositionX, session.Character.PositionY);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Fairy Booster Glace
                case 251:
                    if (!session.Character.Buff.ContainsKey(4003))
                    {
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 4003 });
                        session.CurrentMapInstance?.Broadcast(session.Character.GeneratePairy());
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), inv.Item.Name), 0));
                        session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 3014), session.Character.PositionX, session.Character.PositionY);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Rainbow Pearl/Magic Eraser
                case 666:
                    if (EffectValue == 1 && byte.TryParse(packetsplit[9], out byte islot))
                    {
                        ItemInstance wearInstance = session.Character.Inventory.LoadBySlotAndType(islot, InventoryType.Equipment);

                        if (wearInstance != null && (wearInstance.Item.ItemType == ItemType.Weapon || wearInstance.Item.ItemType == ItemType.Armor) && wearInstance.ShellEffects.Count != 0 && !wearInstance.Item.IsHeroic)
                        {
                            wearInstance.ShellEffects.Clear();
                            wearInstance.ShellRarity = null;
                            DAOFactory.ShellEffectDAO.DeleteByEquipmentSerialId(wearInstance.EquipmentSerialId);
                            if (wearInstance.EquipmentSerialId == Guid.Empty)
                            {
                                wearInstance.EquipmentSerialId = Guid.NewGuid();
                            }
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("OPTION_DELETE"), 0));
                        }
                    }
                    else
                    {
                        session.SendPacket("guri 18 0");
                    }
                    break;

                // Atk/Def/HP/Exp potions
                case 6600:
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                // Ancelloan's Blessing
                case 208:
                    if (!session.Character.Buff.ContainsKey(121))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 121 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Ancelloan's Blessing + 200% xp
                case 209:
                    if (!session.Character.Buff.ContainsKey(375))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 375 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Guardian Angel's Blessing
                case 210:
                    if (!session.Character.Buff.ContainsKey(122))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 122 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                case 2081:
                    if (!session.Character.Buff.ContainsKey(146))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 146 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                // Divorce letter
                case 6969:
                    if (session.Character.Group != null)
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NOT_ALLOWED_IN_GROUP"), 0));
                        return;
                    }
                    CharacterRelationDTO rel = session.Character.CharacterRelations.FirstOrDefault(s => s.RelationType == CharacterRelationType.Spouse);
                    if (rel != null)
                    {
                        session.Character.DeleteRelation(rel.CharacterId == session.Character.CharacterId ? rel.RelatedCharacterId : rel.CharacterId, CharacterRelationType.Spouse);
                        session.SendPacket(UserInterfaceHelper.GenerateInfo(Language.Instance.GetMessageFromKey("DIVORCED")));
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                // Cupid's arrow
                case 34:
                    if (packetsplit != null && packetsplit.Length > 3)
                    {
                        if (long.TryParse(packetsplit[3], out long characterId))
                        {
                            if (session.Character.CharacterId == characterId)
                            {
                                return;
                            }
                            if (session.Character.CharacterRelations.Any(s => s.RelationType == CharacterRelationType.Spouse))
                            {
                                session.SendPacket($"info {Language.Instance.GetMessageFromKey("ALREADY_MARRIED")}");
                                return;
                            }
                            if (session.Character.Group != null)
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NOT_ALLOWED_IN_GROUP"), 0));
                                return;
                            }
                            if (!session.Character.IsFriendOfCharacter(characterId))
                            {
                                session.SendPacket($"info {Language.Instance.GetMessageFromKey("MUST_BE_FRIENDS")}");
                                return;
                            }
                            ClientSession otherSession = ServerManager.Instance.GetSessionByCharacterId(characterId);
                            if (otherSession != null)
                            {
                                if (otherSession.Character.Group != null)
                                {
                                    session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("OTHER_PLAYER_IN_GROUP"), 0));
                                    return;
                                }
                                otherSession.SendPacket(UserInterfaceHelper.GenerateDialog(
                                    $"#fins^34^{session.Character.CharacterId} #fins^69^{session.Character.CharacterId} {string.Format(Language.Instance.GetMessageFromKey("MARRY_REQUEST"), session.Character.Name)}"));
                                session.Character.MarryRequestCharacters.Add(characterId);
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            }
                        }
                    }
                    break;


                case 100: // Miniland Signpost
                    {
                        if (session.Character.BattleEntity.GetOwnedNpcs().Any(s => session.Character.BattleEntity.IsSignpost(s.NpcVNum)))
                        {
                            return;
                        }
                        if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance && new short[] { 1, 145 }.Contains(session.CurrentMapInstance.Map.MapId))
                        {
                            MapNpc signPost = new MapNpc
                            {
                                NpcVNum = (short)EffectValue,
                                MapX = session.Character.PositionX,
                                MapY = session.Character.PositionY,
                                MapId = session.CurrentMapInstance.Map.MapId,
                                ShouldRespawn = false,
                                IsMoving = false,
                                MapNpcId = session.CurrentMapInstance.GetNextNpcId(),
                                Owner = session.Character.BattleEntity,
                                Dialog = 10000,
                                Position = 2,
                                Name = $"{session.Character.Name}'s^[Miniland]"
                            };
                            switch (EffectValue)
                            {
                                case 1428:
                                case 1499:
                                case 1519:
                                    signPost.AliveTime = 3600;
                                    break;

                                default:
                                    signPost.AliveTime = 1800;
                                    break;
                            }
                            signPost.Initialize(session.CurrentMapInstance);
                            session.CurrentMapInstance.AddNPC(signPost);
                            session.CurrentMapInstance.Broadcast(signPost.GenerateIn());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    break;

                case 550: // Campfire and other craft npcs
                    {
                        if (session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance)
                        {
                            short dialog = 10023;
                            switch (EffectValue)
                            {
                                case 956:
                                    dialog = 10023;
                                    break;

                                case 957:
                                    dialog = 10024;
                                    break;

                                case 959:
                                    dialog = 10026;
                                    break;
                            }
                            MapNpc campfire = new MapNpc
                            {
                                NpcVNum = (short)EffectValue,
                                MapX = session.Character.PositionX,
                                MapY = session.Character.PositionY,
                                MapId = session.CurrentMapInstance.Map.MapId,
                                ShouldRespawn = false,
                                IsMoving = false,
                                MapNpcId = session.CurrentMapInstance.GetNextNpcId(),
                                Owner = session.Character.BattleEntity,
                                Dialog = dialog,
                                Position = 2,
                            };
                            campfire.AliveTime = 180;
                            campfire.Initialize(session.CurrentMapInstance);
                            session.CurrentMapInstance.AddNPC(campfire);
                            session.CurrentMapInstance.Broadcast(campfire.GenerateIn());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    break;

                // Faction Egg
                case 570:
                    if (session.Character.Faction == (FactionType)EffectValue)
                    {
                        return;
                    }
                    if (EffectValue < 3)
                    {
                        session.SendPacket(session.Character.Family == null
                            ? $"qna #guri^750^{EffectValue} {Language.Instance.GetMessageFromKey($"ASK_CHANGE_FACTION{EffectValue}")}"
                            : UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("IN_FAMILY"),
                            0));
                    }
                    else
                    {
                        session.SendPacket(session.Character.Family != null
                            ? $"qna #guri^750^{EffectValue} {Language.Instance.GetMessageFromKey($"ASK_CHANGE_FACTION{EffectValue}")}"
                            : UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_FAMILY"),
                            0));
                    }
                    break;
                
                // WINGS SKINS
                case 655:
                    ItemInstance spInstance = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                    if (session.Character.UseSp && spInstance != null && !session.Character.IsSeal)
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_WINGS_CHANGE")}");
                        }
                        else
                        {
                            spInstance.Design = (byte)EffectValue;
                            session.Character.MorphUpgrade2 = EffectValue;
                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                            session.SendPacket(session.Character.GenerateStat());
                            session.SendPackets(session.Character.GenerateStatChar());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_SP"), 0));
                    }
                    break;

                // SP Wings
                case 650:
                    ItemInstance specialistInstance = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                    if (session.Character.UseSp && specialistInstance != null && !session.Character.IsSeal)
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_WINGS_CHANGE")}");
                        }
                        else
                        {
                            void disposeBuff(short vNum)
                            {
                                if (session.Character.BuffObservables.ContainsKey(vNum))
                                {
                                    session.Character.BuffObservables[vNum].Dispose();
                                    session.Character.BuffObservables.Remove(vNum);
                                }
                                session.Character.RemoveBuff(vNum);
                            }
                            disposeBuff(332);
                            disposeBuff(725);
                            disposeBuff(453);
                            disposeBuff(248);
                            disposeBuff(374);
                            disposeBuff(162);
                            disposeBuff(243);
                            disposeBuff(17);
                            disposeBuff(491);
                            disposeBuff(479);
                            disposeBuff(74);
                            disposeBuff(328);
                            disposeBuff(170);
                            disposeBuff(96);
                            disposeBuff(47);
                            disposeBuff(46);
                            disposeBuff(45);
                            disposeBuff(725);
                            disposeBuff(759);
                            disposeBuff(80);
                            disposeBuff(171);
                            disposeBuff(242);
                            disposeBuff(207);
                            disposeBuff(442);
                            disposeBuff(403);
                            disposeBuff(166);
                            disposeBuff(163);
                            disposeBuff(132);
                            disposeBuff(120);
                            disposeBuff(396);
                            disposeBuff(139);
                            disposeBuff(138);
                            disposeBuff(331);
                            disposeBuff(387);
                            disposeBuff(395);
                            disposeBuff(396);
                            disposeBuff(397);
                            disposeBuff(398);
                            disposeBuff(410);
                            disposeBuff(411);
                            disposeBuff(444);
                            disposeBuff(663);
                            disposeBuff(686);
                            disposeBuff(707);
                            disposeBuff(712);
                            disposeBuff(4000);
                            disposeBuff(4001);
                            disposeBuff(4002);
                            disposeBuff(755);
                            disposeBuff(6969);
                            disposeBuff(8016);
                            disposeBuff(838);
                            disposeBuff(723);
                            disposeBuff(704);
                            disposeBuff(705);
                            disposeBuff(697);
                            disposeBuff(851);
                            specialistInstance.Design = (byte)EffectValue;
                            specialistInstance.WingEffect = (byte)EffectValue;
                            session.Character.MorphUpgrade2 = EffectValue;
                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                            session.SendPacket(session.Character.GenerateStat());
                            session.SendPackets(session.Character.GenerateStatChar());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_SP"), 0));
                    }
                    break;

                case 656:
                    ItemInstance OmegaWings = session.Character.Inventory.LoadBySlotAndType((byte)EquipmentType.Sp, InventoryType.Wear);
                    List<int> EvolveWings = new List<int>
                    {
                        16,
                    }; 
                    if (session.Character.UseSp && OmegaWings != null && !session.Character.IsSeal && EvolveWings.Contains(OmegaWings.WingEffect))
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_WINGS_EVO")}");
                        }
                        else
                        {
                            void disposeBuff(short vNum)
                            {
                                if (session.Character.BuffObservables.ContainsKey(vNum))
                                {
                                    session.Character.BuffObservables[vNum].Dispose();
                                    session.Character.BuffObservables.Remove(vNum);
                                }
                                session.Character.RemoveBuff(vNum);
                            }

                            disposeBuff(387);
                            disposeBuff(395);
                            disposeBuff(396);
                            disposeBuff(397);
                            disposeBuff(398);
                            disposeBuff(410);
                            disposeBuff(411);
                            disposeBuff(851);
                            disposeBuff(444);
                            disposeBuff(663);
                            disposeBuff(686);
                            disposeBuff(4000);
                            disposeBuff(4001);
                            disposeBuff(4002);
                            disposeBuff(755);
                            disposeBuff(6969);
                            disposeBuff(8016);
                            
                            OmegaWings.WingEffect += 16;
                            session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                            session.SendPacket(session.Character.GenerateStat());
                            session.SendPackets(session.Character.GenerateStatChar());
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("NO_SP_NO_EV"), 0));
                    }
                    break;

                // Self-Introduction
                case 203:
                    if (!session.Character.IsVehicled && Option == 0)
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateGuri(10, 2, session.Character.CharacterId, 1));
                    }
                    break;

                // Magic Lamp
                case 651:
                    if (session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("ASK_USE")}");
                        }
                        else
                        {
                            session.Character.ChangeSex();
                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                    }
                    break;

                case 5018: // Wedding Box
                    {
                        session.Character.GiftAdd(1981, 1);
                        session.Character.GiftAdd(988, 1);
                        session.Character.GiftAdd(988, 1);
                        session.Character.GiftAdd(984, 1);
                        session.Character.GiftAdd(984, 1);
                        session.Character.GiftAdd(1984, 10);
                        session.Character.GiftAdd(1986, 10);
                        session.Character.GiftAdd(1988, 10);
                        session.Character.GiftAdd(4346, 1);
                        session.Character.GiftAdd(4347, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }


                //Kostüm Boxen

                case 5445: // Lässige Kostümkiste (w)
                    {
                        short[] vnums =
                        vnums = new short[] { 932, 195, 958, 753, 891, 639, 641, 637, 635, 633, 631, 629, 694, 697, 726, 729, 624, 626 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 18);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5446: // Tierische Kostümkiste (w)
                    {
                        short[] vnums =
                        vnums = new short[] { 732, 735, 738, 741, 744, 747, 750, 784, 787, 790, 793, 796, 799, 806, 809, 625, 627 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 17);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5447: // Lockere Kostümkiste (m)
                    {
                        short[] vnums =
                        vnums = new short[] { 927, 189, 954, 894, 646, 644, 642, 640, 638, 636, 634, 632, 630, 628, 667, 670, 624, 626 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 18);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5448: // Animalische Kostümkiste (m)
                    {
                        short[] vnums =
                        vnums = new short[] { 673, 676, 679, 682, 685, 688, 691, 812, 818, 821, 824, 827, 815, 830, 833, 625, 627 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 17);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5079: // Zufallstruhe mit Santa Buschi-Kostüm
                    {
                        short[] vnums =
                        vnums = new short[] { 5080, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5682: // Returning Hero
                    {
                        session.Character.GiftAdd(1286, 2);
                        session.Character.GiftAdd(1285, 2);
                        session.Character.GiftAdd(1362, 2);
                        session.Character.GiftAdd(1246, 6);
                        session.Character.GiftAdd(1247, 6);
                        session.Character.GiftAdd(1248, 6);
                        session.Character.GiftAdd(1249, 6);
                        session.Character.GiftAdd(1011, 50);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1565: // Survival Set
                    {
                        session.Character.GiftAdd(1286, 1);
                        session.Character.GiftAdd(1285, 1);
                        session.Character.GiftAdd(1362, 1);
                        session.Character.GiftAdd(1246, 6);
                        session.Character.GiftAdd(1247, 6);
                        session.Character.GiftAdd(1248, 6);
                        session.Character.GiftAdd(1249, 6);
                        session.Character.GiftAdd(1211, 10);
                        session.Character.GiftAdd(1244, 30);
                        session.Character.GiftAdd(1120, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5358: // Zaubergewand des Lichts
                    {
                        session.Character.GiftAdd(4185, 1);
                        session.Character.GiftAdd(4181, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5052: // Aqua Buschi-Kostümset
                    {
                        session.Character.GiftAdd(4065, 1);
                        session.Character.GiftAdd(4211, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5412: // Rosarotes Partyset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4219, 1);
                        session.Character.GiftAdd(4225, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5413: // Gelbes Partyset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4220, 1);
                        session.Character.GiftAdd(4226, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5414: // Blaues Partyset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4221, 1);
                        session.Character.GiftAdd(4227, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5415: // Rotes Detektivset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4237, 1);
                        session.Character.GiftAdd(4231, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5416: // Goldenes Detektivset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4238, 1);
                        session.Character.GiftAdd(4232, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5417: // Braunes Detektivset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4239, 1);
                        session.Character.GiftAdd(4233, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5599: // Polizeiuniformset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4285, 1);
                        session.Character.GiftAdd(4283, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5716: // Teufelskostüm-Set
                    {
                        session.Character.GiftAdd(4409, 1);
                        session.Character.GiftAdd(4411, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5604: // Portier-Uniformset (unbegrenzt)
                    {
                        session.Character.GiftAdd(4289, 1);
                        session.Character.GiftAdd(4287, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5639: // Tänzerinnen-Set (unbegrenzt)
                    {
                        session.Character.GiftAdd(4323, 1);
                        session.Character.GiftAdd(4319, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5878: // Nussknacker-Kostüm Set (unbegrenzt)
                    {
                        session.Character.GiftAdd(4827, 1);
                        session.Character.GiftAdd(4829, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5732: // Osterhasen-Partykostüm-Truhe
                    {
                        session.Character.GiftAdd(4425, 1);
                        session.Character.GiftAdd(4421, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5486: // Niedliches Tigerkostüm Set
                    {
                        session.Character.GiftAdd(4244, 1);
                        session.Character.GiftAdd(4252, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5816: // Frosthexen Kostüm Set
                    {
                        session.Character.GiftAdd(4534, 1);
                        session.Character.GiftAdd(4536, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5359: // Zauberrobe des Schattens
                    {
                        session.Character.GiftAdd(4187, 1);
                        session.Character.GiftAdd(4183, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5789: // Tropen Kostüm Set
                    {
                        session.Character.GiftAdd(4529, 1);
                        session.Character.GiftAdd(4527, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5220: // Drachenritter-Kostüm Set
                    {
                        session.Character.GiftAdd(4380, 1);
                        session.Character.GiftAdd(4334, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 13305: // Viking Costume Set
                    {
                        session.Character.GiftAdd(4301, 1);
                        session.Character.GiftAdd(4303, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 13303: // Aqua Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4064, 1);
                        session.Character.GiftAdd(4065, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 13304: // Illusionist Costume Set
                    {
                        session.Character.GiftAdd(4258, 1);
                        session.Character.GiftAdd(4260, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                
                case 1517: // Dalmatian costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(783, 1);
                        session.Character.GiftAdd(835, 1);
                    }
                    break;
              
                case 1518: // Rottweiler costume box set (Perm)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(787, 1);
                        session.Character.GiftAdd(837, 1);
                    }
                    break;
                
                case 1519: // Cat siamois costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(789, 1);
                        session.Character.GiftAdd(841, 1);
                    }
                    break;
                
                case 1520: // Blue russian costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(793, 1);
                        session.Character.GiftAdd(845, 1);
                    }
                    break;
                
                case 1521: // Dark lion costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(808, 1);
                        session.Character.GiftAdd(856, 1);
                    }
                    break;
                
                case 1522: // Light lion costume box set (Perm)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(806, 1);
                        session.Character.GiftAdd(854, 1);
                    }
                    break;
                
                case 1523: // Bouledog costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(811, 1);
                        session.Character.GiftAdd(859, 1);
                    }
                    break;
                
                case 1524: // St Bernard costume box set (Perm)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(815, 1);
                        session.Character.GiftAdd(863, 1);
                    }
                    break;
                
                case 1525: // Birman cat costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(817, 1);
                        session.Character.GiftAdd(865, 1);
                    }
                    break;
                
                case 1526: // Korat costume box set (Perm)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(821, 1);
                        session.Character.GiftAdd(869, 1);
                    }
                    break;
                
                case 1527: // Black lion costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(832, 1);
                        session.Character.GiftAdd(880, 1);
                    }
                    break;
                
                case 1528: // Gold lion costume box set (Perm)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(830, 1);
                        session.Character.GiftAdd(878, 1);
                    }
                    break;
                
                case 1529: // Mars hare costume box set (30 Days)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(823, 1);
                        session.Character.GiftAdd(871, 1);
                    }
                    break;
               
                case 1530: // White mars hare costume box set (Perm)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(827, 1);
                        session.Character.GiftAdd(875, 1);
                    }
                    break;
                
                case 1531: // Mars hare costume box set (30 Days)
                    { 
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(798, 1);
                        session.Character.GiftAdd(850, 1);
                    }
                    break;
               
                case 1532: // Mars hare costume box set (Perm)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(796, 1);
                        session.Character.GiftAdd(848, 1);
                    }
                    break;

                case 5610: // Viking Costume Set
                    {
                        session.Character.GiftAdd(4301, 1);
                        session.Character.GiftAdd(4303, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5051: // Aqua Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4064, 1);
                        session.Character.GiftAdd(4065, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5080: // Christmas Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4074, 1);
                        session.Character.GiftAdd(4077, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5183: // Black Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4107, 1);
                        session.Character.GiftAdd(4114, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5184: // Blue Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4108, 1);
                        session.Character.GiftAdd(4115, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5185: // Green Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4109, 1);
                        session.Character.GiftAdd(4116, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5186: // Red Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4110, 1);
                        session.Character.GiftAdd(4117, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5187: // Pink Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4111, 1);
                        session.Character.GiftAdd(4118, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5188: // Light blue Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4112, 1);
                        session.Character.GiftAdd(4119, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5189: // Yellow Bushtail Costume Set
                    {
                        session.Character.GiftAdd(4113, 1);
                        session.Character.GiftAdd(4120, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5190: // Classic Bushtail Costume Set
                    {
                        session.Character.GiftAdd(970, 1);
                        session.Character.GiftAdd(972, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5267: // Cute rabbit (h) Costume Set
                    {
                        session.Character.GiftAdd(4138, 1);
                        session.Character.GiftAdd(4146, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5268: // Cute rabbit (f) Costume Set
                    {
                        session.Character.GiftAdd(4142, 1);
                        session.Character.GiftAdd(4150, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5592: // Illusionist Costume Set
                    {
                        session.Character.GiftAdd(4266, 1);
                        session.Character.GiftAdd(4268, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5302: // Fox Oto Costume Set
                    {
                        session.Character.GiftAdd(4177, 1);
                        session.Character.GiftAdd(4179, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5487: // White tiger Costume Set
                    {
                        session.Character.GiftAdd(4248, 1);
                        session.Character.GiftAdd(4256, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5733: // Easter Rabbit Costume Set
                    {
                        session.Character.GiftAdd(4429, 1);
                        session.Character.GiftAdd(4433, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5737: // Fairy costume Set
                    {
                        session.Character.GiftAdd(4439, 1);
                        session.Character.GiftAdd(4441, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                case 5572: // Illusionist Costume Set
                    {
                        session.Character.GiftAdd(4258, 1);
                        session.Character.GiftAdd(4260, 1);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                
                case 5441: // Football costume pack permanant
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(4195, 1);
                        session.Character.GiftAdd(4196, 1);
                    }
                    break;

                case 5483: // Tolle Tigerkostüme & mehr
                    {
                        short[] vnums =
                        vnums = new short[] { 5487, 5486, 5302, 5051, 2282, 1030 };
                        byte[] counts = { 1, 1, 1, 1, 25, 25 };
                        int item = ServerManager.RandomNumber(0, 6);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5304: // Aqua Buschi Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 5051, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5265: // Zufallstruhe mit flauschigem Hasenkostüm für Männer
                    {
                        short[] vnums =
                        vnums = new short[] { 5267, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5266: // Zufallstruhe mit flauschigem Hasenkostüm für Frauen
                    {
                        short[] vnums =
                        vnums = new short[] { 5268, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                // Mounts

                case 1926: // Truhe des magischen Roller
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(1906, 1);
                    }
                    break;

                case 1927: // Truhe des magischen Teppichs
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(1907, 1);
                    }
                    break;

                case 5297: // Magischer Roller-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 1906, 2160, 1119, 284, 1285, 1904, 1296, 1945 };
                        byte[] counts = { 1, 40, 1, 1, 16, 1, 10, 14 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5298: // Magischer Teppich-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 1907, 2160, 1119, 284, 1285, 1904, 1296, 1945 };
                        byte[] counts = { 1, 40, 1, 1, 16, 1, 10, 14 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5043: // Zufallstruhe des magischen weißen Tigers
                    {
                        short[] vnums =
                        vnums = new short[] { 1965, 1904, 1296, 1286, 1218, 1122, 2282, 1219, 1119 };
                        byte[] counts = { 1, 2, 10, 6, 2, 90, 80, 2, 2 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5030: // Zufallstruhe mit magischem Cabrio
                    {
                        short[] vnums =
                        vnums = new short[] { 5008, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5122: // Zufallstruhe des Fluffy McFly
                    {
                        short[] vnums =
                        vnums = new short[] { 5117, 5119, 1904, 1285, 284, 1119, 2160, 1296 };
                        byte[] counts = { 1, 20, 1, 16, 1, 1, 40, 10 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5154: // Zufallstruhe des Hornfegers
                    {
                        short[] vnums =
                        vnums = new short[] { 5152, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5181: // Winnies Schatztruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 5173, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5198: // Nossis Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 5196, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5254: // Bike-Zufallstruhe "Billy Boneshaker"
                    {
                        short[] vnums =
                        vnums = new short[] { 5232, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5255: // Inlineskates-Zufallstruhe "Blazing Blades"
                    {
                        short[] vnums =
                        vnums = new short[] { 5234, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5256: // Skateboard-Zufallstruhe "Doni Darkslide"
                    {
                        short[] vnums =
                        vnums = new short[] { 5236, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5230: // Zufallstruhe mit magischen Ski
                    {
                        short[] vnums =
                        vnums = new short[] { 5239, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5231: // Zufallstruhe mit magischem Snowboard
                    {
                        short[] vnums =
                        vnums = new short[] { 5241, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5325: // Zauberhafte Einhorn-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 5319, 5321, 5323, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 1, 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 12);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5410: // Farbenfrohe Jeep-Truhe
                    {
                        short[] vnums =
                        vnums = new short[] { 5386, 5387, 5388, 5389, 5390, 5391, 1244, 1363, 1364, 1218, 5369, 1246, 1247, 1248, 1945 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 15);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5843: // Dr.Maunz Truhe
                    {
                        short[] vnums =
                        vnums = new short[] { 1246, 1247, 1248, 2282, 1030, 1904, 1012, 1286, 1945, 1011 };
                        byte[] counts = { 2, 2, 2, 10, 10, 1, 50, 1, 5, 20 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5361: // Truhe mit Windsurfer
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5360, 1);
                    }
                    break;

                case 5713: // Truhe mit magischem Schlitten
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5712, 1);
                    }
                    break;

                case 5744: // "Magische Esel-Piñata"-Truhe
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5743, 1);
                    }
                    break;

                case 5808: // Alpaka-Truhe zum 12. Jahrestag
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5806, 1);
                    }
                    break;

                case 5809: // Rennalpaka-Truhe zum 12. Jahrestag
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5807, 1);
                    }
                    break;

                case 5835: // Truhe des magischen Jaguars
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5834, 1);
                    }
                    break;

                case 5915: // Truhe mit magischem Kamel (Quest)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5914, 1);
                    }
                    break;

                case 5998: // Truhe des magischen Knochendrachen
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(5997, 1);
                    }
                    break;

                //Pets

                case 5019: // Aqua Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 888, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5020: // Kammerjäger Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 399 ,1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5021: // Krankes Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 447, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5022: // Braunes Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 397, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5023: // Samurai Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 192, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5024: // Stier Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 980, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5026: // Rusty Robby Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4063, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5044: // Gladiator Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 398, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5045: // Boxer Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 943, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5046: // Super Kürbis Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4066, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5047: // Santa Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 444, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5048: // Fußball Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4061, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5049: // Schiri Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4062, 1904, 1296, 1286, 1218, 1122, 1219, 1119, 2282 };
                        byte[] counts = { 1, 2, 10, 6, 2, 80, 2, 2, 80 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5106: // Zufallstruhe des süßen Hasen
                    {
                        short[] vnums =
                        vnums = new short[] { 930, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5111: // Chick Norris-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4084, 1296, 1118, 2160, 1904 };
                        byte[] counts = { 1, 10, 1, 40, 1 };
                        int item = ServerManager.RandomNumber(0, 5);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5172: // Piraten Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4101, 4103, 4104, 4105, 2160, 1119, 284, 1285, 1904, 1296, 1945 };
                        byte[] counts = { 1, 1, 1, 1, 40, 1, 1, 16, 1, 10, 14 };
                        int item = ServerManager.RandomNumber(0, 11);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5202: // Shogun Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4125, 2160, 1119, 284, 1285, 1904, 1296, 1945 };
                        byte[] counts = { 1, 40, 1, 1, 16, 1, 10, 14 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5224: // Rudolf Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4127, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5246: // Centurio Buschi-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4126, 2160, 1119, 284, 1285, 1904, 1296, 1945 };
                        byte[] counts = { 1, 40, 1, 1, 16, 1, 10, 14 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5280: // Pegasus-Truhe
                    {
                        short[] vnums =
                        vnums = new short[] { 929, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5285: // Darkos höllische Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4167, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 10);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5294: // Fibis frostige Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4168, 2160, 1119, 284, 1285, 1904, 1296, 1945 };
                        byte[] counts = { 1, 40, 1, 1, 16, 1, 10, 14 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5311: // Rudis Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4197, 4198, 1244, 1363, 1364, 1218, 5369, 1945, 1246, 1247, 1248 };
                        byte[] counts = { 1, 10, 1, 1, 1, 1, 5, 5, 5, 5 };
                        int item = ServerManager.RandomNumber(0, 11);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5317: // Zufallstruhe des Keinarmhasen
                    {
                        short[] vnums =
                        vnums = new short[] { 4199, 2160, 1119, 284, 1285, 1904, 1296, 1945 };
                        byte[] counts = { 1, 40, 1, 1, 16, 1, 10, 14 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5440: // Die Truhe der Ballkünstler
                    {
                        short[] vnums =
                        vnums = new short[] { 5441, 4152, 4153, 4154, 4155, 4156, 4157, 4158, 4159, 4160, 4061, 1030, 2282, 4062 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 25, 25, 1 };
                        int item = ServerManager.RandomNumber(0, 14);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;

                    }

                // Cosmetic Stuff

                case 1878: // Unbekanntes Relikt
                    {
                        short[] vnums =
                        vnums = new short[] { 4056, 4057, 4058, 4059, 4060, 2348, 2349 };
                        byte[] counts = { 1, 1, 1, 1, 1, 2, 2 };
                        int item = ServerManager.RandomNumber(0, 7);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1879: // Mysteriöses Relikt
                    {
                        short[] vnums =
                        vnums = new short[] { 4727, 4728, 4745, 4746, 4763, 4001, 4003, 4005, 4007, 4009, 4011, 4013, 4016, 4019 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 14);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1583: // Glückstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 1286, 1249, 1248, 1247, 1246, 1904, 9324, 1242, 1243, 1244, 2160 };
                        byte[] counts = { 2, 2, 2, 2, 1, 1, 1, 5, 5, 5, 2  };
                        int item = ServerManager.RandomNumber(0, 11);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1568: // Friseur-Set
                    {
                        short[] vnums =
                        vnums = new short[] { 2050, 2051, 2052, 2053, 2054, 2055, 2056, 2057, 2058, 2163, 2164, 2165, 2166, 2167 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
                        int item = ServerManager.RandomNumber(0, 14);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5451: // Unisex-Hutschachtel
                    {
                        short[] vnums =
                        vnums = new short[] { 204, 215, 216, 220, 337, 338, 339, 340, 341, 342, 343, 346 };
                        byte[] counts = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 12);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1497: // Truhe der Spezialistenverbesserung
                    {
                        short[] vnums =
                        vnums = new short[] { 2283, 2284, 2285 };
                        byte[] counts = { 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 3);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                // Göttliche Feen Boxen

                case 5014: // Elkaim-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4129, 1904, 1296, 1286, 1218, 1122, 2282, 1119 };
                        byte[] counts = { 1, 2, 10, 6, 2, 90, 80, 2 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5015: // Ladine-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4130, 1904, 1296, 1286, 1218, 1122, 2282, 1119 };
                        byte[] counts = { 1, 2, 10, 6, 2, 90, 80, 2 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }
                    
                case 5016: // Rumial-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4131, 1904, 1296, 1286, 1218, 1122, 2282, 1119 };
                        byte[] counts = { 1, 2, 10, 6, 2, 90, 80, 2 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5017: // Varik-Zufallstruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 4132, 1904, 1296, 1286, 1218, 1122, 2282, 1119 };
                        byte[] counts = { 1, 2, 10, 6, 2, 90, 80, 2 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1098: // Geheimnisvolle Box 
                    {
                        short[] vnums =
                        vnums = new short[] { 1246, 1247, 1248, 1249, 2282, 1030, 1242, 1243, 1244, 1363, 1364, 1218, 5369 };
                        byte[] counts = { 2, 2, 2, 2, 10, 10, 5, 5, 5, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 13);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1910: // Geheimnisvolle Box A
                    {
                        short[] vnums =
                        vnums = new short[] { 1286, 1249, 1901, 1900, 4991, 9324 };
                        byte[] counts = { 1, 2, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 6);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1911: // Geheimnisvolle Box B
                    {
                        short[] vnums =
                        vnums = new short[] { 1011, 1122, 1242, 1243, 1244, 1880 };
                        byte[] counts = { 50, 20, 5, 5, 5, 100 };
                        int item = ServerManager.RandomNumber(0, 6);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1572: // Siebte mysteriöse Schatztruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 2511, 2512, 2513 };
                        byte[] counts = { 5, 3, 1 };
                        int item = ServerManager.RandomNumber(0, 3);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 1571: // Siebte legendäre Schatztruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 2282, 1030, 1363, 1364 };
                        byte[] counts = { 10, 10, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 4);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;
                    }

                case 5989: // laurena box
                    {
                        short[] vnums =
                        vnums = new short[] { 4813, 4812, 4699, 2282, 2285, 2397, 2396, 2514, 2515, 2516, 2517, 2518, 2519, 2520, 2521, 2434, 4835, 4836, 4837 };
                        byte[] counts = { 1, 1, 1, 25, 3, 5, 5, 15, 15, 15, 15, 15, 15, 15, 15, 1, 1, 1, 1 };
                        int item = ServerManager.RandomNumber(0, 19);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;

                    }

                case 5968: // Halloween-Schatztruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 1246, 1247, 1248, 1249, 5358, 5359, 2323, 2282, 2282, 2282, 1030, 1030, 1030, 1244, 1244, 1244 };
                        byte[] counts = { 5, 5, 5, 2, 1, 1, 1, 25, 50, 75, 25, 50, 75, 10, 20, 30 };
                        int item = ServerManager.RandomNumber(0, 16);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;

                    }

                case 5909: // Geschenkbeutel der Akamurhändler
                    {
                        short[] vnums =
                        vnums = new short[] { 5929, 5929, 5929, 5929, 5916, 5916, 5916, 5916, 5916 };
                        byte[] counts = { 5, 10, 15, 20, 1, 2, 3, 4, 5 };
                        int item = ServerManager.RandomNumber(0, 9);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;

                    }

                case 1949: // Versiegelte Schatztruhe
                    {
                        short[] vnums =
                        vnums = new short[] { 5931, 5932, 1286, 1242, 1243, 1244, 1296, 1134 };
                        byte[] counts = { 1, 1, 2, 5, 5, 5, 2, 1 };
                        int item = ServerManager.RandomNumber(0, 8);
                        session.Character.GiftAdd(vnums[item], counts[item]);
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        break;

                    }

                case 1508: // Present Chest
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(9020, 12);
                        session.Character.GiftAdd(9021, 12);
                        session.Character.GiftAdd(9022, 12);
                        session.Character.GiftAdd(8317, 1);
                        session.Character.GiftAdd(8317, 1);
                        session.Character.GiftAdd(1098, 5);
                        session.Character.GiftAdd(4134, 1);
                        session.Character.GiftAdd(5483, 5);
                    }
                    break;

                case 1950: // Nikolaus Present Chest
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.GiftAdd(1246, 99);
                        session.Character.GiftAdd(1247, 99);
                        session.Character.GiftAdd(1248, 99);
                        session.Character.GiftAdd(8317, 1);
                        session.Character.GiftAdd(8317, 1);
                        session.Character.GiftAdd(8317, 1);
                        session.Character.GiftAdd(1452, 6);
                        session.Character.GiftAdd(4134, 1);
                        session.Character.GiftAdd(2514, 999);
                        session.Character.GiftAdd(2515, 999);
                        session.Character.GiftAdd(2516, 999);
                        session.Character.GiftAdd(2517, 999);
                        session.Character.GiftAdd(2518, 999);
                        session.Character.GiftAdd(2519, 999);
                        session.Character.GiftAdd(2520, 999);
                        session.Character.GiftAdd(2521, 999);
                        session.Character.GiftAdd(1244, 500);
                    }
                    break;

                // Vehicles
                case 1000:
                    if (EffectValue != 0
                     || session.CurrentMapInstance?.MapInstanceType == MapInstanceType.EventGameInstance
                     || session.CurrentMapInstance?.MapInstanceType == (MapInstanceType.TalentArenaMapInstance)
                     || session.CurrentMapInstance?.MapInstanceType == (MapInstanceType.IceBreakerInstance)
                     || session.Character.IsSeal || session.Character.IsMorphed)
                    {
                        return;
                    }
                    short morph = Morph;
                    byte speed = Speed;
                    if (Morph < 0)
                    {
                        switch (VNum)
                        {
                            case 5923:
                                morph = 2513;
                                speed = 14;
                                break;
                        }
                    }
                    if (morph > 0)
                    {
                        if (Option == 0 && !session.Character.IsVehicled)
                        {
                            if (session.Character.Buff.Any(s => s.Card.BuffType == BuffType.Bad))
                            {
                                session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("CANT_TRASFORM_WITH_DEBUFFS"),
                                    0));
                                return;
                            }
                            if (session.Character.IsSitting)
                            {
                                session.Character.IsSitting = false;
                                session.CurrentMapInstance?.Broadcast(session.Character.GenerateRest());
                            }
                            session.Character.LastDelay = DateTime.Now;
                            session.SendPacket(UserInterfaceHelper.GenerateDelay(3000, 3, $"#u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2"));
                        }
                        else
                        {
                            if (!session.Character.IsVehicled && Option != 0)
                            {
                                DateTime delay = DateTime.Now.AddSeconds(-4);
                                if (session.Character.LastDelay > delay && session.Character.LastDelay < delay.AddSeconds(2))
                                {
                                    session.Character.IsVehicled = true;
                                    session.Character.VehicleSpeed = speed;
                                    session.Character.VehicleItem = this;
                                    session.Character.LoadSpeed();
                                    session.Character.MorphUpgrade = 0;
                                    session.Character.MorphUpgrade2 = 0;
                                    session.Character.Morph = morph + (byte)session.Character.Gender;
                                    session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 196), session.Character.PositionX, session.Character.PositionY);
                                    session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                                    session.SendPacket(session.Character.GenerateCond());
                                    session.Character.LastSpeedChange = DateTime.Now;
                                    session.Character.Mates.Where(s => s.IsTeamMember).ToList()
                                        .ForEach(s => session.CurrentMapInstance?.Broadcast(s.GenerateOut()));
                                    if (Morph < 0)
                                    {
                                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    }
                                }
                            }
                            else if (session.Character.IsVehicled)
                            {
                                session.Character.RemoveVehicle();
                                foreach (Mate teamMate in session.Character.Mates.Where(m => m.IsTeamMember))
                                {
                                    teamMate.PositionX =
                                        (short)(session.Character.PositionX + (teamMate.MateType == MateType.Partner ? -1 : 1));
                                    teamMate.PositionY = (short)(session.Character.PositionY + 1);
                                    if (session.Character.MapInstance.Map.IsBlockedZone(teamMate.PositionX, teamMate.PositionY))
                                    {
                                        teamMate.PositionX = session.Character.PositionX;
                                        teamMate.PositionY = session.Character.PositionY;
                                    }
                                    teamMate.UpdateBushFire();
                                    Parallel.ForEach(session.CurrentMapInstance.Sessions.Where(s => s.Character != null), s =>
                                    {
                                        if (ServerManager.Instance.ChannelId != 51 || session.Character.Faction == s.Character.Faction)
                                        {
                                            s.SendPacket(teamMate.GenerateIn(false, ServerManager.Instance.ChannelId == 51));
                                        }
                                        else
                                        {
                                            s.SendPacket(teamMate.GenerateIn(true, ServerManager.Instance.ChannelId == 51, s.Account.Authority));
                                        }
                                    });
                                }
                                session.SendPacket(session.Character.GeneratePinit());
                                session.Character.Mates.ForEach(s => session.SendPacket(s.GenerateScPacket()));
                                session.SendPackets(session.Character.GeneratePst());
                            }
                        }
                    }
                    break;

                // Sealed Vessel
                case 1002:
                    int type, secondaryType, inventoryType, slot;
                    if (packetsplit != null && int.TryParse(packetsplit[2], out type) && int.TryParse(packetsplit[3], out secondaryType) && int.TryParse(packetsplit[4], out inventoryType) && int.TryParse(packetsplit[5], out slot))
                    {
                        int packetType;
                        switch (EffectValue)
                        {
                            case 69:
                                if (int.TryParse(packetsplit[6], out packetType))
                                {
                                    switch (packetType)
                                    {
                                        case 0:
                                            session.SendPacket(UserInterfaceHelper.GenerateDelay(5000, 7, $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                            break;

                                        case 1:
                                            int rnd = ServerManager.RandomNumber(0, 1000);
                                            if (rnd < 5)
                                            {
                                                short[] vnums =
                                                {
                                                        5560, 5591, 4099, 907, 1160, 4705, 4706, 4707, 4708, 4709, 4710, 4711, 4712, 4713, 4714,
                                                        4715, 4716
                                                    };
                                                byte[] counts = { 1, 1, 1, 1, 10, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
                                                int item = ServerManager.RandomNumber(0, 17);
                                                session.Character.GiftAdd(vnums[item], counts[item]);
                                            }
                                            else if (rnd < 30)
                                            {
                                                short[] vnums = { 361, 362, 363, 366, 367, 368, 371, 372, 373 };
                                                session.Character.GiftAdd(vnums[ServerManager.RandomNumber(0, 9)], 1);
                                            }
                                            else
                                            {
                                                short[] vnums =
                                                {
                                                        1161, 2282, 1030, 1244, 1218, 5369, 1012, 1363, 1364, 2160, 2173, 5959, 5983, 2514,
                                                        2515, 2516, 2517, 2518, 2519, 2520, 2521, 1685, 1686, 5087, 5203, 2418, 2310, 2303,
                                                        2169, 2280, 5892, 5893, 5894, 5895, 5896, 5897, 5898, 5899, 5332, 5105, 2161, 2162
                                                    };
                                                byte[] counts =
                                                {
                                                        10, 10, 20, 5, 1, 1, 99, 1, 1, 5, 5, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 5, 20,
                                                        20, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
                                                    };
                                                int item = ServerManager.RandomNumber(0, 42);
                                                session.Character.GiftAdd(vnums[item], counts[item]);
                                            }
                                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                            break;
                                    }
                                }
                                break;



                            default:
                                if (int.TryParse(packetsplit[6], out packetType))
                                {
                                    if (session.Character.MapInstance.Map.MapTypes.Any(s => s.MapTypeId == (short)MapTypeEnum.Act4))
                                    {
                                        return;
                                    }
                                    switch (packetType)
                                    {
                                        case 0:
                                            session.SendPacket(UserInterfaceHelper.GenerateDelay(2000, 7, $"#u_i^{type}^{secondaryType}^{inventoryType}^{slot}^1"));
                                            break;

                                        case 1:
                                            if (session.HasCurrentMapInstance && (session.Character.MapInstance == session.Character.Miniland || session.CurrentMapInstance.MapInstanceType == MapInstanceType.BaseMapInstance) && (session.Character.LastVessel.AddSeconds(1) <= DateTime.Now || session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.FastVessels)))
                                            {
                                                short[] vnums = { 1386, 1387, 1388, 1389, 1390, 1391, 1392, 1393, 1394, 1395, 1396, 1397, 1398, 1399, 1400, 1401, 1402, 1403, 1404, 1405 };
                                                short vnum = vnums[ServerManager.RandomNumber(0, 20)];

                                                NpcMonster npcmonster = ServerManager.GetNpcMonster(vnum);
                                                if (npcmonster == null)
                                                {
                                                    return;
                                                }
                                                MapMonster monster = new MapMonster
                                                {
                                                    MonsterVNum = vnum,
                                                    MapX = session.Character.PositionX,
                                                    MapY = session.Character.PositionY,
                                                    MapId = session.Character.MapInstance.Map.MapId,
                                                    Position = session.Character.Direction,
                                                    IsMoving = true,
                                                    MapMonsterId = session.CurrentMapInstance.GetNextMonsterId(),
                                                    ShouldRespawn = false
                                                };
                                                monster.Initialize(session.CurrentMapInstance);
                                                session.CurrentMapInstance.AddMonster(monster);
                                                session.CurrentMapInstance.Broadcast(monster.GenerateIn());
                                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                                session.Character.LastVessel = DateTime.Now;
                                            }
                                            break;
                                    }
                                }
                                break;
                        }
                    }
                    break;

                // Golden Bazaar Medal
                case 1003:
                    if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.BazaarMedalGold || s.StaticBonusType == StaticBonusType.BazaarMedalSilver))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.BazaarMedalGold
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Silver Bazaar Medal
                case 1004:
                    if (!session.Character.StaticBonusList.Any(s => s.StaticBonusType == StaticBonusType.BazaarMedalGold || s.StaticBonusType == StaticBonusType.BazaarMedalGold))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.BazaarMedalSilver
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Pet Slot Expansion
                case 1006:
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey("ASK_PET_MAX")}");
                    }
                    else if ((inv.Item?.IsSoldable == true && session.Character.MaxMateCount < 90) || session.Character.MaxMateCount < 30)
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.MaxMateCount += 10;
                        session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("GET_PET_PLACES"), 10));
                        session.SendPacket(session.Character.GenerateScpStc());
                    }
                    break;

                // Permanent Backpack Expansion
                case 601:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.BackPack))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddYears(15),
                            StaticBonusType = StaticBonusType.BackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Permanent Partner's Backpack
                case 602:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.PetBackPack))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddYears(15),
                            StaticBonusType = StaticBonusType.PetBackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Permanent Pet Basket
                case 603:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.PetBasket))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddYears(15),
                            StaticBonusType = StaticBonusType.PetBasket
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket("ib 1278 1");
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Pet Basket
                case 1007:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.PetBasket))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.PetBasket
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket("ib 1278 1");
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Partner's Backpack
                case 1008:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.PetBackPack))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.PetBackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                // Backpack Expansion
                case 1009:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.BackPack))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddDays(EffectValue),
                            StaticBonusType = StaticBonusType.BackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 12));
                    }
                    break;

                    ////Expansion Ticket
                case 605:
                    if (session.Character.StaticBonusList.All(s => s.StaticBonusType != StaticBonusType.BackPack))
                    {
                        session.Character.StaticBonusList.Add(new StaticBonusDTO
                        {
                            CharacterId = session.Character.CharacterId,
                            DateEnd = DateTime.Now.AddYears(15),
                            StaticBonusType = StaticBonusType.BackPack
                        });
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.SendPacket(session.Character.GenerateExts());
                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("EFFECT_ACTIVATED"), Name), 36));
                    }
                    break;

                case 5833:
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    switch (ServerManager.RandomNumber(0, 6))
                    {
                        case 0:
                            session.Character.GiftAdd(4129, 1);
                            break;

                        case 1:
                            session.Character.GiftAdd(4130, 1);
                            break;

                        case 2:
                            session.Character.GiftAdd(4131, 1);
                            break;

                        case 3:
                            session.Character.GiftAdd(4132, 1);
                            break;

                        case 4:
                            session.Character.GiftAdd(2160, 20);
                            break;

                        default:
                            session.Character.GiftAdd(1896, 1);
                            break;
                    }
                    break;

                case 5837:
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    switch (ServerManager.RandomNumber(0, 7))
                    {
                        case 0:
                            session.Character.GiftAdd(2282, 20);
                            break;

                        case 1:
                            session.Character.GiftAdd(1286, 1);
                            break;

                        case 2:
                            session.Character.GiftAdd(4181, 1);
                            break;

                        case 3:
                            session.Character.GiftAdd(4380, 1);
                            break;

                        case 4:
                            session.Character.GiftAdd(1012, 1);
                            break;

                        case 5:
                            session.Character.GiftAdd(5834, 1);
                            break;

                        default:
                            session.Character.GiftAdd(1030, 20);
                            break;
                    }
                    break;

                case 5836:
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    switch (ServerManager.RandomNumber(0, 7))
                    {
                        case 0:
                            session.Character.GiftAdd(5783, 5);
                            break;

                        case 1:
                            session.Character.GiftAdd(5499, 1);
                            break;

                        case 2:
                            session.Character.GiftAdd(5056, 20);
                            break;

                        case 3:
                            session.Character.GiftAdd(5777, 5);
                            break;

                        case 4:
                            session.Character.GiftAdd(5560, 1);
                            break;

                        case 5:
                            session.Character.GiftAdd(5776, 5);
                            break;

                        default:
                            session.Character.GiftAdd(4401, 1);
                            break;
                    }
                    break;

                case 1501: //wk
                    if (!session.Character.Buff.ContainsKey(151) && !session.Character.Buff.ContainsKey(153) && !session.Character.Buff.ContainsKey(155) && !session.Character.Buff.ContainsKey(152))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 151 });
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 153 });
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 155 });
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 152 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                case 1502: //holy
                    if (!session.Character.Buff.ContainsKey(91) && !session.Character.Buff.ContainsKey(89))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 91 });
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 89 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                case 1503: //sader
                    if (!session.Character.Buff.ContainsKey(139) && !session.Character.Buff.ContainsKey(138))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 139 });
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 138 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                case 1504: //krieger
                    if (!session.Character.Buff.ContainsKey(72) && !session.Character.Buff.ContainsKey(71) && !session.Character.Buff.ContainsKey(93))
                    {
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 72 });
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 71 });
                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 93 });
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                    }
                    break;

                //Clicklik shell kutu
                case 2727:
                    int crnd4 = ServerManager.RandomNumber(0, 1000); // Lütfen buna dokunma.
                    int crare4 = (sbyte)ServerManager.RandomNumber(4, 8); // Min Rare , Max Rare
                    int cupgrade4 = ServerManager.RandomNumber(88, 95); // Min Level  , Max Level
                    short[] cvnums4 = null;
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to push the limits?")}");
                    }
                    else return;

                    if (crnd4 < 550) // Şans oranı 1000 de 650 gibi düşün
                    {
                        cvnums4 = new short[] { 565, 566, 567, 568, 569, 570, 574, 575, 576, 571, 572, 573 };
                    }
                    else
                    {
                        cvnums4 = new short[] { 577, 578, 579, 580, 581, 582, 586, 587, 588, 583, 584, 585 };
                    }
                    session.Character.Inventory.AddNewToInventory(cvnums4[ServerManager.RandomNumber(0, 12)], 1, InventoryType.Equipment, (sbyte)crare4, (byte)cupgrade4);
                    if (crare4 == 7)
                    {
                        session.SendPacket("msg 3 You got a Rarity 7 Shell. Let's define it");
                    }
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                //Clicklik shell kutu
                case 2626:
                    int crnd3 = ServerManager.RandomNumber(0, 1000); // Lütfen buna dokunma.
                    int crare3 = (sbyte)ServerManager.RandomNumber(7, 8); // Min Rare , Max Rare
                    int cupgrade3 = ServerManager.RandomNumber(93, 99); // Min Level  , Max Level
                    short[] cvnums3 = null;
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to push the limits?")}");
                    }
                    else return;

                    if (crnd3 < 450) // Şans oranı 1000 de 650 gibi düşün
                    {
                        cvnums3 = new short[] { 571, 572, 573 };
                    }
                    else
                    {
                        cvnums3 = new short[] { 583, 584, 585 };
                    }
                    session.Character.Inventory.AddNewToInventory(cvnums3[ServerManager.RandomNumber(0, 3)], 1, InventoryType.Equipment, (sbyte)crare3, (byte)cupgrade3);
                    if (crare3 == 7)
                    {
                        session.SendPacket("msg 3 You got a Rarity 7 Shell. Let's define it");
                    }
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                //Clicklik shell kutu
                case 2525:
                    int crnd2 = ServerManager.RandomNumber(0, 1000); // Lütfen buna dokunma.
                    int crare2 = (sbyte)ServerManager.RandomNumber(7, 8); // Min Rare , Max Rare
                    int cupgrade2 = ServerManager.RandomNumber(93, 97); // Min Level  , Max Level
                    short[] cvnums2 = null;
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to push the limits?")}");
                    }
                    else return;

                    if (crnd2 < 550) // Şans oranı 1000 de 650 gibi düşün
                    {
                        cvnums2 = new short[] {  571, 572, 573 };
                    }
                    else
                    {
                        cvnums2 = new short[] {  583, 584, 585 };
                    }
                    session.Character.Inventory.AddNewToInventory(cvnums2[ServerManager.RandomNumber(0, 3)], 1, InventoryType.Equipment, (sbyte)crare2, (byte)cupgrade2);
                    if (crare2 == 7)
                    {
                        session.SendPacket("msg 3 You got a Rarity 7 Shell. Let's define it");
                    }
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;


                //Clicklik shell kutu
                case 2424:
                    int crnd1 = ServerManager.RandomNumber(0, 1000); // Lütfen buna dokunma.
                    int crare1 = (sbyte)ServerManager.RandomNumber(7, 8); // Min Rare , Max Rare
                    int cupgrade1 = ServerManager.RandomNumber(88, 95); // Min Level  , Max Level
                    short[] cvnums1 = null;
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to push the limits?")}");
                    }
                    else return;

                    if (crnd1 < 550) // Şans oranı 1000 de 650 gibi düşün
                    {
                        cvnums1 = new short[] { 565, 566, 567, 568, 569, 570, 574, 575, 576, 571, 572, 573 };
                    }
                    else
                    {
                        cvnums1 = new short[] { 577, 578, 579, 580, 581, 582, 586, 587, 588, 583, 584, 585 };
                    }
                    session.Character.Inventory.AddNewToInventory(cvnums1[ServerManager.RandomNumber(0, 12)], 1, InventoryType.Equipment, (sbyte)crare1, (byte)cupgrade1);
                    if (crare1 == 7)
                    {
                        session.SendPacket("msg 3 You got a Rarity 7 Shell. Let's define it");
                    }
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                //Clicklik shell kutu
                case 2323:
                    int crnd = ServerManager.RandomNumber(0, 1000); // Lütfen buna dokunma.
                    int crare = (sbyte)ServerManager.RandomNumber(6, 8); // Min Rare , Max Rare
                    int cupgrade = ServerManager.RandomNumber(88, 95); // Min Level  , Max Level
                    short[] cvnums = null;
                    if (Option == 0)
                    {
                        session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to push the limits?")}");
                    }
                    else return;

                    if (crnd < 650) // Şans oranı 1000 de 650 gibi düşün
                    {
                        cvnums = new short[] { 565, 566, 567, 568, 569, 570,574,575,576,571,572,573 };
                    }
                    else
                    {
                        cvnums = new short[] {577,578,579,580,581,582,586,587,588,583,584,585 };
                    }
                    session.Character.Inventory.AddNewToInventory(cvnums[ServerManager.RandomNumber(0, 12)], 1,InventoryType.Equipment, (sbyte)crare, (byte)cupgrade);
                    if (crare==7)
                    {
                        session.SendPacket("msg 3 You got a Rarity 7 Shell. Let's define it");
                    }
                   session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                // Changeclass
                case 1461:
                    if (session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to change your class to swordsman?")}");
                        }
                        else
                        {
                            session.Character.ChangeClass((ClassType.Swordsman), true);

                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                    }
                    break;

                case 1570:
                    {
                        int PrestigeLevel = session.Character.PrestigeLevel;
                        if (PrestigeLevel >= 1)
                        {
                            if (PrestigeLevel < 3)
                            {
                                session.Character.PrestigeLevel = 3;
                                session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 6), session.Character.PositionX, session.Character.PositionY);
                                session.CurrentMapInstance?.Broadcast(StaticPacketHelper.GenerateEff(UserType.Player, session.Character.CharacterId, 198), session.Character.PositionX, session.Character.PositionY);
                                session.SendPacket(session.Character.GenerateLev());
                                session.CurrentMapInstance?.Broadcast(session, session.Character.GenerateIn(), ReceiverType.AllExceptMe);
                                session.CurrentMapInstance?.Broadcast(session, session.Character.GenerateGidx(), ReceiverType.AllExceptMe);
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            }
                            else
                            {
                                UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("You are already PrestigeLevel 1!"), 0);
                            }

                        }
                        else
                        {
                            UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("You need atleast PrestigeLevel 1 to use this!"), 0);
                        }
                        break;
                    }

                // Changeclass
                case 2382:
                    if (session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to change your class to archer?")}");
                        }
                        else
                        {
                            session.Character.ChangeClass((ClassType.Archer), true);

                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                    }
                    break;
                // Changeclass
                case 2383:
                    if (session.Character.Inventory.All(i => i.Type != InventoryType.Wear))
                    {
                        if (Option == 0)
                        {
                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^3 {Language.Instance.GetMessageFromKey("Do you want to change your class to mage?")}");
                        }
                        else
                        {
                            session.Character.ChangeClass((ClassType.Magician), true);

                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                        }
                    }
                    else
                    {
                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("EQ_NOT_EMPTY"), 0));
                    }
                    break;

                // Sealed Tarot Card
                case 1005:
                    session.Character.GiftAdd((short)(VNum - Effect), 1);
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                // Tarot Card Game
                case 1894:
                    if (EffectValue == 0)
                    {
                        for (int i = 0; i < 5; i++)
                        {
                            session.Character.GiftAdd((short)(Effect + ServerManager.RandomNumber(0, 10)), 1);
                        }
                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    }
                    break;

                // Sealed Tarot Card
                case 2152:
                    session.Character.GiftAdd((short)(VNum + Effect), 1);
                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                    break;

                // Transformation scrolls
                case 1001:
                    if (session.Character.IsMorphed)
                    {
                        session.Character.IsMorphed = false;
                        session.Character.Morph = 0;
                        session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                    }
                    else if (!session.Character.UseSp && !session.Character.IsVehicled)
                    {
                        if (Option == 0)
                        {
                            session.Character.LastDelay = DateTime.Now;
                            session.SendPacket(UserInterfaceHelper.GenerateDelay(3000, 3, $"#u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^1"));
                        }
                        else
                        {
                            int[] possibleTransforms = null;

                            switch (EffectValue)
                            {
                                case 1: // Halloween
                                    possibleTransforms = new int[]
                                    {
                                    404, //Torturador pellizcador
                                    405, //Torturador enrollador
                                    406, //Torturador de acero
                                    446, //Guerrero yak
                                    447, //Mago yak
                                    441, //Guerrero de la muerte
                                    276, //Rey polvareda
                                    324, //Princesa Catrisha
                                    248, //Bruja oscura
                                    249, //Bruja de sangre
                                    438, //Bruja blanca fuerte
                                    236, //Guerrero esqueleto
                                    245, //Sombra nocturna
                                    439, //Guerrero esqueleto resucitado
                                    272, //Arquero calavera
                                    274, //Guerrero calavera
                                    2691, //Frankenstein
                                    };
                                    break;

                                case 2: // Ice Costume
                                    break;

                                case 3: // Bushtail Costume
                                    break;
                            }

                            if (possibleTransforms != null)
                            {
                                session.Character.IsMorphed = true;
                                session.Character.Morph = 1000 + possibleTransforms[ServerManager.RandomNumber(0, possibleTransforms.Length)];
                                session.CurrentMapInstance?.Broadcast(session.Character.GenerateCMode());
                                if (VNum != 1914)
                                {
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                }
                            }
                        }
                    }
                    break;

                default:
                    switch (EffectValue)
                    {
                        // Angel Base Flag
                        case 965:

                        // Demon Base Flag
                        case 966:
                            if (ServerManager.Instance.ChannelId == 51 && session.CurrentMapInstance?.Map.MapId != 130 && session.CurrentMapInstance?.Map.MapId != 131 && EffectValue - 964 == (short)session.Character.Faction)
                            {
                                session.CurrentMapInstance?.SummonMonster(new MonsterToSummon((short)EffectValue, new MapCell { X = session.Character.PositionX, Y = session.Character.PositionY }, null, false, isHostile: false, aliveTime: 1800));
                                session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                            }
                            break;

                        default:
                            switch (VNum)
                            {
                                case 5856: // Partner Slot Expansion
                                case 9113: // Partner Slot Expansion (Limited)
                                    {
                                        if (Option == 0)
                                        {
                                            session.SendPacket($"qna #u_i^1^{session.Character.CharacterId}^{(byte)inv.Type}^{inv.Slot}^2 {Language.Instance.GetMessageFromKey("ASK_PARTNER_MAX")}");
                                        }
                                        else if (session.Character.MaxPartnerCount < 12)
                                        {
                                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                            session.Character.MaxPartnerCount++;
                                            session.SendPacket(session.Character.GenerateSay(Language.Instance.GetMessageFromKey("GET_PARTNER_PLACES"), 10));
                                            session.SendPacket(session.Character.GenerateScpStc());
                                        }
                                    }
                                    break;

                                

                                case 5931: // Tique de habilidad de compañero (una)
                                case 9109:
                                    {
                                        var partner = session.Character.Mates.FirstOrDefault(s => s.IsTeamMember && s.MateType == MateType.Partner);
                                        if (packetsplit == null)
                                        {
                                            // Packet Hacking
                                            return;
                                        }

                                        if (packetsplit.Length < 9)
                                        {
                                            // Packet hacking
                                            return;
                                        }

                                        if (!byte.TryParse(packetsplit[9], out byte sPos))
                                        {
                                            // out of range
                                            return;
                                        }

                                        if (!Enum.TryParse(packetsplit[8], out EquipmentType sEqpType))
                                        {
                                            // Out of range
                                            return;
                                        }

                                        if (!byte.TryParse(packetsplit[6], out byte sRequest))
                                        {
                                            return;
                                        }

                                        if (partner == null)
                                        {
                                            return;
                                        }

                                        if (partner.Sp == null)
                                        {
                                            return;
                                        }

                                        if (partner.IsUsingSp)
                                        {
                                            return;
                                        }

                                        if (partner.Sp.RemoveSkill(sPos))
                                        {
                                            session.Character.Inventory.RemoveItemFromInventory(inv.Id);

                                            partner.Sp.ReloadSkills();
                                            partner.Sp.FullXp();

                                            session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("PSP_SKILL_RESETTED"), 1));
                                        }

                                        session.SendPacket(partner.GenerateScPacket());
                                    }
                                    break;

                                case 5932: // Tique de habilidad de compañero (todas)
                                case 9110:
                                    {
                                        var partner = session.Character.Mates.FirstOrDefault(s => s.IsTeamMember && s.MateType == MateType.Partner);
                                        if (packetsplit == null)
                                        {
                                            // Packet Hacking
                                            return;
                                        }

                                        if (packetsplit.Length < 9)
                                        {
                                            // Packet hacking
                                            return;
                                        }

                                        if (!byte.TryParse(packetsplit[9], out byte pos))
                                        {
                                            // out of range
                                            return;
                                        }

                                        if (!Enum.TryParse(packetsplit[8], out EquipmentType eqpType))
                                        {
                                            // Out of range
                                            return;
                                        }

                                        if (!byte.TryParse(packetsplit[6], out byte request))
                                        {
                                            return;
                                        }

                                        if (partner == null)
                                        {
                                            return;
                                        }

                                        if (partner.Sp == null)
                                        {
                                            return;
                                        }

                                        if (partner.IsUsingSp)
                                        {
                                            return;
                                        }

                                        if (partner.Sp.GetSkillsCount() < 1)
                                        {
                                            return;
                                        }

                                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);

                                        partner.Sp.ClearSkills();
                                        partner.Sp.FullXp();

                                        session.SendPacket(UserInterfaceHelper.GenerateModal(Language.Instance.GetMessageFromKey("PSP_ALL_SKILLS_RESETTED"), 1));

                                        session.SendPacket(partner.GenerateScPacket());
                                    }

                                    break;

                                // Template Boxes
                                case 21101:
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    session.Character.GiftAdd(21100, (byte)ServerManager.RandomNumber(15, 20));
                                    break;

                                case 21102:
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    session.Character.GiftAdd(21100, (byte)ServerManager.RandomNumber(5, 10));
                                    break;

                                case 21103:
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    session.Character.GiftAdd(21100, (byte)ServerManager.RandomNumber(15, 20));
                                    break;

                                case 21104:
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    session.Character.GiftAdd(21100, (byte)ServerManager.RandomNumber(15, 50));
                                    break;

                                // Event Upgrade Scrolls
                                case 5107:
                                    session.SendPacket("wopen 35 0 0");
                                    break;
                                case 5207:
                                    session.SendPacket("wopen 38 0 0");
                                    break;
                                case 5519:
                                    session.SendPacket("wopen 42 0 0");
                                    break;
                                case 10000:
                                    if (EffectValue != 0)
                                    {
                                        if (session.Character.IsSitting)
                                        {
                                            session.Character.IsSitting = false;
                                            session.SendPacket(session.Character.GenerateRest());
                                        }
                                        session.SendPacket(UserInterfaceHelper.GenerateGuri(12, 1, session.Character.CharacterId, EffectValue));
                                    }
                                    break;

                                // Martial Artist Starter Pack
                                case 5832:
                                    {
                                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);

                                        // Steel Fist
                                        session.Character.GiftAdd(4756, 1, 5);

                                        // Trainee Martial Artist's Uniform
                                        session.Character.GiftAdd(4757, 1, 5);

                                        // Mystical Glacier Stone
                                        session.Character.GiftAdd(4504, 1);

                                        // Hero's Amulet of Fire
                                        session.Character.GiftAdd(4503, 1);

                                        // Fairy Fire/Water/Light/Dark (30%)
                                        for (short itemVNum = 884; itemVNum <= 887; itemVNum++)
                                        {
                                            session.Character.GiftAdd(itemVNum, 1);
                                        }
                                    }
                                    break;

                                // Soulstone Blessing
                                case 1362:
                                case 5195:
                                case 5211:
                                case 9075:
                                    if (!session.Character.Buff.ContainsKey(146))
                                    {
                                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                        session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 146 });
                                    }
                                    else
                                    {
                                        session.SendPacket(UserInterfaceHelper.GenerateMsg(Language.Instance.GetMessageFromKey("ITEM_IN_USE"), 0));
                                    }
                                    break;

                                case 1428:
                                    session.SendPacket("guri 18 1");
                                    break;

                                case 1429:
                                    session.SendPacket("guri 18 0");
                                    break;

                                case 1904:
                                    short[] items = { 1894, 1895, 1896, 1897, 1898, 1899, 1900, 1901, 1902, 1903 };
                                    for (int i = 0; i < 5; i++)
                                    {
                                        session.Character.GiftAdd(items[ServerManager.RandomNumber(0, items.Length)], 1);
                                    }
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    break;

                                case 5370:
                                    if (session.Character.Buff.Any(s => s.Card.CardId == 393))
                                    {
                                        session.SendPacket(session.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("ALREADY_GOT_BUFF"), session.Character.Buff.FirstOrDefault(s => s.Card.CardId == 393)?.Card.Name), 10));
                                        return;
                                    }
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    session.Character.AddStaticBuff(new StaticBuffDTO { CardId = 393 });
                                    break;

                                case 5841:
                                    int rnd = ServerManager.RandomNumber(0, 1000);
                                    short[] vnums = null;
                                    if (rnd < 900)
                                    {
                                        vnums = new short[] { 4356, 4357, 4358, 4359 };
                                    }
                                    else
                                    {
                                        vnums = new short[] { 4360, 4361, 4362, 4363 };
                                    }
                                    session.Character.GiftAdd(vnums[ServerManager.RandomNumber(0, 4)], 1);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    break;

                                case 5916:
                                case 5927:
                                    session.Character.AddStaticBuff(new StaticBuffDTO
                                    {
                                        CardId = 340,
                                        CharacterId = session.Character.CharacterId,
                                        RemainingTime = 7200
                                    });
                                    session.Character.RemoveBuff(339);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    break;

                                case 12004:
                                    session.Character.AddStaticBuff(new StaticBuffDTO
                                    {
                                        CardId = 4004,
                                        CharacterId = session.Character.CharacterId,
                                        RemainingTime = 601
                                    });
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    break;

                                case 5929:
                                case 5930:
                                    session.Character.AddStaticBuff(new StaticBuffDTO
                                    {
                                        CardId = 340,
                                        CharacterId = session.Character.CharacterId,
                                        RemainingTime = 600
                                    });
                                    session.Character.RemoveBuff(339);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    break;

                                case 1910: // Geheimnisvolle Box A (Starter Paket)
                                    {
                                        session.Character.GiftAdd(800, 1);
                                        session.Character.GiftAdd(801, 1);
                                        session.Character.GiftAdd(802, 1);
                                        session.Character.GiftAdd(803, 1);
;                                       session.Character.GiftAdd(4167, 1);
                                        session.Character.GiftAdd(4246, 1);
                                        session.Character.GiftAdd(4254, 1); 
                                        session.Character.GiftAdd(1272, 1);
                                        session.Character.GiftAdd(1011, 99);
                                        session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                        break;
                                    }

                                // Mother Nature's Rune Pack (limited)
                                case 9117:
                                    rnd = ServerManager.RandomNumber(0, 1000);
                                    vnums = null;
                                    if (rnd < 900)
                                    {
                                        vnums = new short[] { 8312, 8313, 8314, 8315 };
                                    }
                                    else
                                    {
                                        vnums = new short[] { 8316, 8317, 8318, 8319 };
                                    }
                                    session.Character.GiftAdd(vnums[ServerManager.RandomNumber(0, 4)], 1);
                                    session.Character.Inventory.RemoveItemFromInventory(inv.Id);
                                    break;

                                default:
                                    IEnumerable<RollGeneratedItemDTO> roll = DAOFactory.RollGeneratedItemDAO.LoadByItemVNum(VNum);
                                    IEnumerable<RollGeneratedItemDTO> rollGeneratedItemDtos = roll as IList<RollGeneratedItemDTO> ?? roll.ToList();
                                    if (!rollGeneratedItemDtos.Any())
                                    {
                                        Logger.Warn(string.Format(Language.Instance.GetMessageFromKey("NO_HANDLER_ITEM"), GetType(), VNum, Effect, EffectValue));
                                        return;
                                    }
                                    int probabilities = rollGeneratedItemDtos.Where(s => s.Probability != 10000).Sum(s => s.Probability);
                                    int rnd2 = ServerManager.RandomNumber(0, probabilities);
                                    int currentrnd = 0;
                                    foreach (RollGeneratedItemDTO rollitem in rollGeneratedItemDtos.Where(s => s.Probability == 10000))
                                    {
                                        sbyte rare = 0;
                                        if (rollitem.IsRareRandom)
                                        {
                                            rnd = ServerManager.RandomNumber(0, 100);

                                            for (int j = ItemHelper.RareRate.Length - 1; j >= 0; j--)
                                            {
                                                if (rnd < ItemHelper.RareRate[j])
                                                {
                                                    rare = (sbyte)j;
                                                    break;
                                                }
                                            }
                                            if (rare < 1)
                                            {
                                                rare = 1;
                                            }
                                        }
                                        session.Character.GiftAdd(rollitem.ItemGeneratedVNum, rollitem.ItemGeneratedAmount, (byte)rare, design: rollitem.ItemGeneratedDesign);
                                    }
                                    foreach (RollGeneratedItemDTO rollitem in rollGeneratedItemDtos.Where(s => s.Probability != 10000).OrderBy(s => ServerManager.RandomNumber()))
                                    {
                                        sbyte rare = 0;
                                        if (rollitem.IsRareRandom)
                                        {
                                            rnd = ServerManager.RandomNumber(0, 100);

                                            for (int j = ItemHelper.RareRate.Length - 1; j >= 0; j--)
                                            {
                                                if (rnd < ItemHelper.RareRate[j])
                                                {
                                                    rare = (sbyte)j;
                                                    break;
                                                }
                                            }
                                            if (rare < 1)
                                            {
                                                rare = 1;
                                            }
                                        }

                                        currentrnd += rollitem.Probability;
                                        if (currentrnd < rnd2)
                                        {
                                            continue;
                                        }
                                        /*if (rollitem.IsSuperReward)
                                        {
                                            CommunicationServiceClient.Instance.SendMessageToCharacter(new SCSCharacterMessage
                                            {
                                                DestinationCharacterId = null,
                                                SourceCharacterId = session.Character.CharacterId,
                                                SourceWorldId = ServerManager.Instance.WorldId,
                                                Message = Language.Instance.GetMessageFromKey("SUPER_REWARD"),
                                                Type = MessageType.Shout
                                            });
                                        }*/
                                        session.Character.GiftAdd(rollitem.ItemGeneratedVNum, rollitem.ItemGeneratedAmount, (byte)rare, design: rollitem.ItemGeneratedDesign);//, rollitem.ItemGeneratedUpgrade);
                                        break;
                                    }
                                    session.Character.Inventory.RemoveItemAmount(VNum);
                                    break;
                            }
                            break;
                    }
                    break;
            }
            session.Character.IncrementQuests(QuestType.Use, inv.ItemVNum);
        }

        #endregion
    }
}