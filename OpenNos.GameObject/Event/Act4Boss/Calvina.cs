﻿using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenNos.GameObject.Event.Act4Boss
{
    class Calvina
    {
        #region Properties

        public static int AngelDamage { get; set; }

        public static MapInstance CalvinaMapInstance { get; set; }

        public static int DemonDamage { get; set; }

        public static bool IsLocked { get; set; }

        public static bool IsRunning { get; set; }

        public static int RemainingTime { get; set; }

        public static MapInstance CalvinaStartMap { get; set; }

        #endregion

        #region Methods

        public static void Run()
        {
            CalvinaBossRaidThread raidThread = new CalvinaBossRaidThread();
            Observable.Timer(TimeSpan.FromMinutes(0)).Subscribe(X => raidThread.Run());
        }

        #endregion

        public class CalvinaBossRaidThread
        {
            #region Methods
            public void Run()
            {
                Calvina.RemainingTime = 3600;
                const int interval = 3;

                Calvina.CalvinaMapInstance = ServerManager.GenerateMapInstance(140, MapInstanceType.CalvinaBoss, new InstanceBag());
                Calvina.CalvinaStartMap = ServerManager.GetMapInstance(ServerManager.GetBaseMapInstanceIdByMapId(134));

                Calvina.CalvinaMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 140,
                    SourceX = 3,
                    SourceY = 47,
                    DestinationMapId = 134,
                    DestinationX = 135,
                    DestinationY = 95,
                    Type = 20
                });

                Calvina.CalvinaStartMap.CreatePortal(new Portal
                {
                    SourceMapId = 134,
                    SourceX = 135,
                    SourceY = 95,
                    DestinationMapInstanceId = Calvina.CalvinaMapInstance.MapInstanceId,
                    DestinationX = 3,
                    DestinationY = 47,
                    Type = 20
                });

                List<EventContainer> onDeathEvents = new List<EventContainer>
                {
                    new EventContainer(Calvina.CalvinaMapInstance, EventActionType.SCRIPTEND, (byte)1)
                };

                MapMonster mapMonster = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 20,
                    MapX = 18,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster);

                MapMonster mapMonster1 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 19,
                    MapX = 27,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster1.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster1);

                MapMonster mapMonster2 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 27,
                    MapX = 30,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster2.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster2);

                MapMonster mapMonster3 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 39,
                    MapX = 30,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster3.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster3);

                MapMonster mapMonster4 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 23,
                    MapX = 7,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster4.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster4);

                MapMonster mapMonster5 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 15,
                    MapX = 15,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster5.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster5);

                MapMonster mapMonster6 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 16,
                    MapX = 26,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster6.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster6);

                MapMonster mapMonster7 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 21,
                    MapX = 38,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster7.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster7);

                MapMonster mapMonster8 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 30,
                    MapX = 37,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster8.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster8);

                MapMonster mapMonster9 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 38,
                    MapX = 36,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster9.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster9);

                MapMonster mapMonster10 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 35,
                    MapX = 23,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster10.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster10);

                MapMonster mapMonster11 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 28,
                    MapX = 15,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster11.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster11);

                MapMonster mapMonster12 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 35,
                    MapX = 12,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster12.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster12);

                MapMonster mapMonster13 = new MapMonster
                {
                    MonsterVNum = 625,
                    MapY = 43,
                    MapX = 21,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster13.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(mapMonster13);

                MapMonster BossMonster = new MapMonster
                {
                    MonsterVNum = 629,
                    MapY = 21,
                    MapX = 28,
                    MapId = Calvina.CalvinaMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Calvina.CalvinaMapInstance.GetNextMonsterId(),
                    ShouldRespawn = false
                };
                BossMonster.Initialize(Calvina.CalvinaMapInstance);
                Calvina.CalvinaMapInstance.AddMonster(BossMonster);

                MapMonster Boss = Calvina.CalvinaMapInstance.Monsters.Find(s => s.Monster.NpcMonsterVNum == 629);

                if (Boss != null)
                {
                    Boss.BattleEntity.OnDeathEvents = onDeathEvents;
                    Boss.IsBoss = true;
                }

                ServerManager.Shout(Language.Instance.GetMessageFromKey("CALVINA_OPEN"), true);

                RefreshRaid(Calvina.RemainingTime);

                ServerManager.Instance.CalvinaBossStart = DateTime.Now;

                while (Calvina.RemainingTime > 0)
                {
                    Calvina.RemainingTime -= interval;
                    Thread.Sleep(interval * 1000);
                    RefreshRaid(Calvina.RemainingTime);
                }

                EndRaid();
            }
            private void EndRaid()
            {
                ServerManager.Shout(Language.Instance.GetMessageFromKey("CALVINA_END"), true);

                foreach (Portal p in Calvina.CalvinaStartMap.Portals.Where(s => s.DestinationMapInstanceId == Calvina.CalvinaStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Calvina.CalvinaStartMap.Broadcast(p.GenerateGp());
                    Calvina.CalvinaStartMap.Portals.Remove(p);
                }
                foreach (ClientSession sess in Calvina.CalvinaStartMap.Sessions.ToList())
                {
                    ServerManager.Instance.ChangeMapInstance(sess.Character.CharacterId, Calvina.CalvinaStartMap.MapInstanceId, sess.Character.MapX, sess.Character.MapY);
                    Thread.Sleep(100);
                }
                EventHelper.Instance.RunEventAsync(new EventContainer(Calvina.CalvinaStartMap, EventActionType.DISPOSEMAP, null));
                Calvina.IsRunning = false;
                Calvina.AngelDamage = 0;
                Calvina.DemonDamage = 0;
                ServerManager.Instance.StartedEvents.Remove(EventType.CalvinaBoss);
            }

            private void LockRaid()
            {
                foreach (Portal p in Calvina.CalvinaStartMap.Portals.Where(s => s.DestinationMapInstanceId == Calvina.CalvinaStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Calvina.CalvinaStartMap.Broadcast(p.GenerateGp());
                    p.IsDisabled = false;
                    p.Type = (byte)PortalType.Closed;
                    Calvina.CalvinaStartMap.Broadcast(p.GenerateGp());
                }
                ServerManager.Shout(Language.Instance.GetMessageFromKey("CALVINA_LOCKED"), true);
                Calvina.IsLocked = true;
            }

            private void RefreshRaid(int remaining)
            {
                int maxHp = ServerManager.GetNpcMonster(629).MaxHP;
                Calvina.CalvinaMapInstance.Broadcast(UserInterfaceHelper.GenerateCHDM(maxHp, Calvina.AngelDamage, Calvina.DemonDamage, Calvina.RemainingTime));

                if (((maxHp / 10) * 8 < Calvina.AngelDamage + Calvina.DemonDamage) && !Calvina.IsLocked)
                {
                    LockRaid();
                }
            }
        }
    }
}
#endregion
