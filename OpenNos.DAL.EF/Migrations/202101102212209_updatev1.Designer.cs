﻿// <auto-generated />
namespace OpenNos.DAL.EF.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.4.0")]
    public sealed partial class updatev1 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(updatev1));
        
        string IMigrationMetadata.Id
        {
            get { return "202101102212209_updatev1"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
