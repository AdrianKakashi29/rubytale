﻿using Discord;
using Discord.Webhook;
using OpenNos.Domain;
using System.Threading.Tasks;


namespace OpenNos.GameObject.Discord.ACT4
{
    public class Act4Discord
    {
        public Act4Discord(byte faction, MapInstanceType raidType, bool isMukraju)
        {
            Faction = faction;
            RaidType = raidType;
            IsMukraju = isMukraju;
            MainAsync().GetAwaiter().GetResult();
        }

        public MapInstanceType RaidType { get; set; }

        public byte Faction { get; set; }

        public bool IsMukraju { get; set; }


        public async Task MainAsync()
        {
            //webhook url format https://discordapp.com/api/webhooks/{id}/{token}
            EmbedBuilder embed;
            string nameFaction;
            string message = "";

            using (var client = new DiscordWebhookClient("https://discordapp.com/api/webhooks/789633333713043556/n4s_9SwI0l6e6Bu9s_m5doYA0z444SL6kijHElz6QBH4FQV91mp6MyBr9B728zuuwgaH"))
            {
                switch (Faction)
                {
                    case 1:
                        nameFaction = "Angel";
                        break;
                    case 2:
                        nameFaction = "Demon";
                        break;
                    default:
                        return;

                }
                if (IsMukraju == true)
                {
                    message = "A New Dungeon arrive's soon... Get ready for the Frozen Crown!";
                    string description = $"Raid started for the {nameFaction} faction";
                    embed = new EmbedBuilder
                    {
                        Color = Color.DarkBlue,
                        ImageUrl = "https://nosapki.nostale.club/images/moby/556.png",
                        Title = "Lord Mukraju Appeared",
                        Description = $"Faction {nameFaction}"
                    };
                }
                else
                {
                    message = "A New Dungeon arrived!";
                    string description = $"Raid started for the {nameFaction} faction";
                    switch (RaidType)
                    {
                        case MapInstanceType.Act4Morcos:
                            embed = new EmbedBuilder
                            {
                                Color = Color.Red,
                                ImageUrl = "https://nosapki.nostale.club/images/moby/563.png",
                                Title = "Lord Morcos Raid has been Opened!",
                                Description = description
                            };
                            break;
                        case MapInstanceType.Act4Calvina:
                            embed = new EmbedBuilder
                            {
                                Color = Color.Blue,
                                ImageUrl = "https://nosapki.nostale.club/images/moby/629.png",
                                Title = "Lady Calavinas Raid has been Opened!",
                                Description = description
                            };
                            break;
                        case MapInstanceType.Act4Berios:
                            embed = new EmbedBuilder
                            {
                                Color = Color.Gold,
                                ImageUrl = "https://nosapki.nostale.club/images/moby/624.png",
                                Title = "Lord Berios Raid has been Opened!",
                                Description = description
                            };
                            break;
                        case MapInstanceType.Act4Hatus:
                            embed = new EmbedBuilder
                            {
                                Color = Color.DarkTeal,
                                ImageUrl = "https://nosapki.nostale.club/images/moby/577.png",
                                Title = "Lord Hatus Raid has been Opened!",
                                Description = description
                            };
                            break;
                        default:
                            return;
                    }
                }
                await client.SendMessageAsync(text: message, embeds: new[] { embed.Build() });
            }
        }
    }
}
