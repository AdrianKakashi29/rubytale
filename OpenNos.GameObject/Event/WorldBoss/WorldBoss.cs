﻿using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenNos.GameObject.Event.WorldBoss
{
    class WorldBoss
    {
        #region Properties

        public static int AngelDamage { get; set; }

        public static MapInstance WorldBossMapInstance { get; set; }

        public static int DemonDamage { get; set; }

        public static bool IsLocked { get; set; }

        public static bool IsRunning { get; set; }

        public static int RemainingTime { get; set; }

        public static MapInstance WorldBMapInstance { get; set; }

        #endregion

        #region Methods

        public static void Run()
        {
            WorldBossRaidThread raidThread = new WorldBossRaidThread();
            Observable.Timer(TimeSpan.FromMinutes(0)).Subscribe(X => raidThread.Run());
        }
        #endregion


        public class WorldBossRaidThread
        {
            #region Methods
            public void Run()
            {
                WorldBoss.RemainingTime = 3600;
                const int interval = 3;
                //GenerateMapInstance = Die map wo der Boss sein soll.
                WorldBoss.WorldBossMapInstance = ServerManager.GenerateMapInstance(2500, MapInstanceType.WorldBossInstance, new InstanceBag());
                // GetBaseMapInstanceIdByMapId = Da wo das boss portal sich öffnen soll. Am besten ne map Die leer ist wo das Portal sich öffnet für Worldboss
                WorldBoss.WorldBMapInstance = ServerManager.GetMapInstance(ServerManager.GetBaseMapInstanceIdByMapId(261));

                WorldBoss.WorldBossMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 2500,
                    SourceX = 25,
                    SourceY = 25,
                    DestinationMapId = 261,
                    DestinationX = 65,
                    DestinationY = 65,
                    Type = -1
                });
                /*
                WorldBoss.WorldBossMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 2110,
                    SourceX = 41,
                    SourceY = 43,
                    DestinationMapId = 261,
                    DestinationX = 65,
                    DestinationY = 65,
                    Type = -1
                });
                WorldBoss.WorldBossMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 2110,
                    SourceX = 41,
                    SourceY = 43,
                    DestinationMapId = 261,
                    DestinationX = 65,
                    DestinationY = 65,
                    Type = -1
                });*/
                WorldBoss.WorldBMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 261,
                    SourceX = 65,
                    SourceY = 65,
                    DestinationMapInstanceId = WorldBoss.WorldBossMapInstance.MapInstanceId,
                    DestinationX = 25,
                    DestinationY = 25,
                    Type = -1
                });

                List<EventContainer> onDeathEvents = new List<EventContainer>
                {
                    new EventContainer(WorldBoss.WorldBossMapInstance, EventActionType.SCRIPTEND, (byte)1)
                };

                MapMonster BossMonster = new MapMonster
                {
                    MonsterVNum = 2059,
                    MapY = 23,
                    MapX = 25,
                    MapId = WorldBoss.WorldBossMapInstance.Map.MapId,
                    Position = 2,
                    IsMoving = true,
                    MapMonsterId = WorldBoss.WorldBossMapInstance.GetNextMonsterId(),
                    ShouldRespawn = false
                };
                BossMonster.Initialize(WorldBoss.WorldBossMapInstance);
                WorldBoss.WorldBossMapInstance.AddMonster(BossMonster);

                MapMonster Boss = WorldBoss.WorldBossMapInstance.Monsters.Find(s => s.Monster.NpcMonsterVNum == 2059);

                if (Boss != null)
                {
                    Boss.BattleEntity.OnDeathEvents = onDeathEvents;
                    Boss.IsBoss = true;
                }

                ServerManager.Shout(Language.Instance.GetMessageFromKey("WORLDBOSS_OPEN"), true);

                RefreshRaid(WorldBoss.RemainingTime);

                ServerManager.Instance.WorldBossStart = DateTime.Now;

                while (WorldBoss.RemainingTime > 0)
                {
                    WorldBoss.RemainingTime -= interval;
                    Thread.Sleep(interval * 1000);
                    RefreshRaid(WorldBoss.RemainingTime);
                }

                EndRaid();

            }

            private void EndRaid()
            {
                ServerManager.Shout(Language.Instance.GetMessageFromKey("WORLDBOSS_END"), true);

                foreach (Portal p in WorldBoss.WorldBMapInstance.Portals.Where(s => s.DestinationMapInstanceId == WorldBoss.WorldBMapInstance.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    WorldBoss.WorldBMapInstance.Broadcast(p.GenerateGp());
                    WorldBoss.WorldBMapInstance.Portals.Remove(p);
                }
                foreach (ClientSession sess in WorldBoss.WorldBMapInstance.Sessions.ToList())
                {
                    ServerManager.Instance.ChangeMapInstance(sess.Character.CharacterId, WorldBoss.WorldBMapInstance.MapInstanceId, sess.Character.MapX, sess.Character.MapY);
                    Thread.Sleep(100);
                }
                EventHelper.Instance.RunEventAsync(new EventContainer(WorldBoss.WorldBMapInstance, EventActionType.DISPOSEMAP, null));
                WorldBoss.IsRunning = false;
                WorldBoss.AngelDamage = 0;
                WorldBoss.DemonDamage = 0;
                ServerManager.Instance.StartedEvents.Remove(EventType.WORLDBOSS);
            }

            private void LockRaid()
            {
                foreach (Portal p in WorldBoss.WorldBMapInstance.Portals.Where(s => s.DestinationMapInstanceId == WorldBoss.WorldBMapInstance.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    WorldBoss.WorldBMapInstance.Broadcast(p.GenerateGp());
                    p.IsDisabled = false;
                    p.Type = (byte)PortalType.Closed;
                    WorldBoss.WorldBMapInstance.Broadcast(p.GenerateGp());
                }
                ServerManager.Shout(Language.Instance.GetMessageFromKey("WORLDBOSS_LOCKED"), true);
                WorldBoss.IsLocked = true;
            }

            private void RefreshRaid(int remaining)
            {
                int maxHP = ServerManager.GetNpcMonster(2059).MaxHP;
                WorldBoss.WorldBossMapInstance.Broadcast(UserInterfaceHelper.GenerateCHDM(maxHP, WorldBoss.AngelDamage, WorldBoss.DemonDamage, WorldBoss.RemainingTime));

                if (((maxHP / 10) * 8 < WorldBoss.AngelDamage + WorldBoss.DemonDamage) && !WorldBoss.IsLocked)
                {
                    LockRaid();
                }
            }

        }
    }
}

#endregion