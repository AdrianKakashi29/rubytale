﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatev1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "TattooCount", c => c.Byte(nullable: false));
            AddColumn("dbo.CharacterSkill", "IsTattoo", c => c.Boolean(nullable: false));
            AddColumn("dbo.CharacterSkill", "IsPartnerSkill", c => c.Boolean(nullable: false));
            AddColumn("dbo.CharacterSkill", "TattooUpgrade", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CharacterSkill", "TattooUpgrade");
            DropColumn("dbo.CharacterSkill", "IsPartnerSkill");
            DropColumn("dbo.CharacterSkill", "IsTattoo");
            DropColumn("dbo.Character", "TattooCount");
        }
    }
}
