﻿using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenNos.GameObject.Event.Act4Boss
{
    class Morcos
    {
        #region Properties

        public static int AngelDamage { get; set; }

        public static MapInstance MorcosMapInstance { get; set; }

        public static int DemonDamage { get; set; }

        public static bool IsLocked { get; set; }

        public static bool IsRunning { get; set; }

        public static int RemainingTime { get; set; }

        public static MapInstance MorcosStartMap { get; set; }

        #endregion

        #region Methods

        public static void Run()
        {
            MorcosBossRaidThread raidThread = new MorcosBossRaidThread();
            Observable.Timer(TimeSpan.FromMinutes(0)).Subscribe(X => raidThread.Run());
        }

        #endregion

        public class MorcosBossRaidThread
        {
            #region Methods
            public void Run()
            {
                Morcos.RemainingTime = 3600;
                const int interval = 3;

                Morcos.MorcosMapInstance = ServerManager.GenerateMapInstance(136, MapInstanceType.MorcosBoss, new InstanceBag());
                Morcos.MorcosStartMap = ServerManager.GetMapInstance(ServerManager.GetBaseMapInstanceIdByMapId(134));

                Morcos.MorcosMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 138,
                    SourceX = 56,
                    SourceY = 81,
                    DestinationMapId = 134,
                    DestinationX = 137,
                    DestinationY = 98,
                    Type = 20
                });

                Morcos.MorcosStartMap.CreatePortal(new Portal
                {
                    SourceMapId = 134,
                    SourceX = 137,
                    SourceY = 98,
                    DestinationMapInstanceId = Morcos.MorcosMapInstance.MapInstanceId,
                    DestinationX = 56,
                    DestinationY = 81,
                    Type = 20
                });

                List<EventContainer> onDeathEvents = new List<EventContainer>
                {
                    new EventContainer(Morcos.MorcosMapInstance, EventActionType.SCRIPTEND, (byte)1)
                };

                MapMonster mapMonster = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 11,
                    MapX = 44,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster);

                MapMonster mapMonster1 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 16,
                    MapX = 48,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster1.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster1);

                MapMonster mapMonster2 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 20,
                    MapX = 54,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster2.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster2);

                MapMonster mapMonster3 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 19,
                    MapX = 60,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster3.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster3);

                MapMonster mapMonster4 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 15,
                    MapX = 66,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster4.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster4);

                MapMonster mapMonster5 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 21,
                    MapX = 73,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster5.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster5);

                MapMonster mapMonster6 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 26,
                    MapX = 68,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster6.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster6);

                MapMonster mapMonster7 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 26,
                    MapX = 77,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster7.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster7);

                MapMonster mapMonster8 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 30,
                    MapX = 82,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster8.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster8);

                MapMonster mapMonster9 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 34,
                    MapX = 79,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster9.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster9);

                MapMonster mapMonster10 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 36,
                    MapX = 71,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster10.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster10);

                MapMonster mapMonster11 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 42,
                    MapX = 58,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster11.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster11);

                MapMonster mapMonster12 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 52,
                    MapX = 59,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster12.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster12);

                MapMonster mapMonster13 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 60,
                    MapX = 61,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster13.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster13);

                MapMonster mapMonster14 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 60,
                    MapX = 54,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster14.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster14);

                MapMonster mapMonster15 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 67,
                    MapX = 56,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster15.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster15);

                MapMonster mapMonster16 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 74,
                    MapX = 60,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster16.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster16);

                MapMonster mapMonster17 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 75,
                    MapX = 50,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster17.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster17);

                MapMonster mapMonster18 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 58,
                    MapX = 41,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster18.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster18);

                MapMonster mapMonster19 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 52,
                    MapX = 35,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster19.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster19);

                MapMonster mapMonster20 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 39,
                    MapX = 35,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster20.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster20);

                MapMonster mapMonster21 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 31,
                    MapX = 41,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster21.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster21);

                MapMonster mapMonster22 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 29,
                    MapX = 33,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster22.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster22);

                MapMonster mapMonster23 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 19,
                    MapX = 40,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster23.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster23);

                MapMonster mapMonster24 = new MapMonster
                {
                    MonsterVNum = 562,
                    MapY = 30,
                    MapX = 43,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster24.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(mapMonster24);

                MapMonster BossMonster = new MapMonster
                {
                    MonsterVNum = 563,
                    MapY = 11,
                    MapX = 56,
                    MapId = Morcos.MorcosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = false,
                    MapMonsterId = Morcos.MorcosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = false
                };
                BossMonster.Initialize(Morcos.MorcosMapInstance);
                Morcos.MorcosMapInstance.AddMonster(BossMonster);

                MapMonster Boss = Morcos.MorcosMapInstance.Monsters.Find(s => s.Monster.NpcMonsterVNum == 563);

                if (Boss != null)
                {
                    Boss.BattleEntity.OnDeathEvents = onDeathEvents;
                    Boss.IsBoss = true;
                }

                ServerManager.Shout(Language.Instance.GetMessageFromKey("MORCOS_OPEN"), true);

                RefreshRaid(Morcos.RemainingTime);

                ServerManager.Instance.MorcosBossStart = DateTime.Now;

                while (Morcos.RemainingTime > 0)
                {
                    Morcos.RemainingTime -= interval;
                    Thread.Sleep(interval * 1000);
                    RefreshRaid(Morcos.RemainingTime);
                }

                EndRaid();
            }
            private void EndRaid()
            {
                ServerManager.Shout(Language.Instance.GetMessageFromKey("MORCOS_END"), true);

                foreach (Portal p in Morcos.MorcosStartMap.Portals.Where(s => s.DestinationMapInstanceId == Morcos.MorcosStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Morcos.MorcosStartMap.Broadcast(p.GenerateGp());
                    Morcos.MorcosStartMap.Portals.Remove(p);
                }
                foreach (ClientSession sess in Morcos.MorcosStartMap.Sessions.ToList())
                {
                    ServerManager.Instance.ChangeMapInstance(sess.Character.CharacterId, Morcos.MorcosStartMap.MapInstanceId, sess.Character.MapX, sess.Character.MapY);
                    Thread.Sleep(100);
                }
                EventHelper.Instance.RunEventAsync(new EventContainer(Morcos.MorcosStartMap, EventActionType.DISPOSEMAP, null));
                Morcos.IsRunning = false;
                Morcos.AngelDamage = 0;
                Morcos.DemonDamage = 0;
                ServerManager.Instance.StartedEvents.Remove(EventType.MorcosBoss);
            }

            private void LockRaid()
            {
                foreach (Portal p in Morcos.MorcosStartMap.Portals.Where(s => s.DestinationMapInstanceId == Morcos.MorcosStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Morcos.MorcosStartMap.Broadcast(p.GenerateGp());
                    p.IsDisabled = false;
                    p.Type = (byte)PortalType.Closed;
                    Morcos.MorcosStartMap.Broadcast(p.GenerateGp());
                }
                ServerManager.Shout(Language.Instance.GetMessageFromKey("MORCOS_LOCKED"), true);
                Morcos.IsLocked = true;
            }

            private void RefreshRaid(int remaining)
            {
                int maxHp = ServerManager.GetNpcMonster(629).MaxHP;
                Morcos.MorcosStartMap.Broadcast(UserInterfaceHelper.GenerateCHDM(maxHp, Morcos.AngelDamage, Morcos.DemonDamage, Morcos.RemainingTime));

                if (((maxHp / 10) * 8 < Morcos.AngelDamage + Morcos.DemonDamage) && !Morcos.IsLocked)
                {
                    LockRaid();
                }
            }
        }
    }
}
#endregion