﻿using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using OpenNos.GameObject.Networking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenNos.GameObject.Event.Act4Boss
{
    class Berios
    {
        #region Properties

        public static int AngelDamage { get; set; }

        public static MapInstance BeriosMapInstance { get; set; }

        public static int DemonDamage { get; set; }

        public static bool IsLocked { get; set; }

        public static bool IsRunning { get; set; }

        public static int RemainingTime { get; set; }

        public static MapInstance BeriosStartMap { get; set; }

        #endregion

        #region Methods

        public static void Run()
        {
            BeriosBossRaidThread raidThread = new BeriosBossRaidThread();
            Observable.Timer(TimeSpan.FromMinutes(0)).Subscribe(X => raidThread.Run());
        }

        #endregion

        public class BeriosBossRaidThread
        {
            #region Methods
            public void Run()
            {
                Berios.RemainingTime = 3600;
                const int interval = 3;

                Berios.BeriosMapInstance = ServerManager.GenerateMapInstance(142, MapInstanceType.BeriosBoss, new InstanceBag());
                Berios.BeriosStartMap = ServerManager.GetMapInstance(ServerManager.GetBaseMapInstanceIdByMapId(134));

                Berios.BeriosMapInstance.CreatePortal(new Portal
                {
                    SourceMapId = 140,
                    SourceX = 56,
                    SourceY = 22,
                    DestinationMapId = 134,
                    DestinationX = 140,
                    DestinationY = 99,
                    Type = 20
                });

                Berios.BeriosStartMap.CreatePortal(new Portal
                {
                    SourceMapId = 134,
                    SourceX = 140,
                    SourceY = 99,
                    DestinationMapInstanceId = Berios.BeriosMapInstance.MapInstanceId,
                    DestinationX = 56,
                    DestinationY = 22,
                    Type = 20
                });

                List<EventContainer> onDeathEvents = new List<EventContainer>
                {
                    new EventContainer(Berios.BeriosMapInstance, EventActionType.SCRIPTEND, (byte)1)
                };

                MapMonster mapMonster = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 36,
                    MapX = 28,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster);

                MapMonster mapMonster1 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 38,
                    MapX = 32,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster1.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster1);

                MapMonster mapMonster2 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 34,
                    MapX = 34,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster2.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster2);

                MapMonster mapMonster3 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 34,
                    MapX = 37,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster3.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster3);

                MapMonster mapMonster4 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 30,
                    MapX = 37,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster4.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster4);

                MapMonster mapMonster5 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 27,
                    MapX = 39,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster5.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster5);

                MapMonster mapMonster6 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 27,
                    MapX = 36,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster6.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster6);

                MapMonster mapMonster7 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 20,
                    MapX = 35,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster7.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster7);

                MapMonster mapMonster8 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 21,
                    MapX = 30,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster8.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster8);

                MapMonster mapMonster9 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 19,
                    MapX = 27,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster9.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster9);

                MapMonster mapMonster10 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 22,
                    MapX = 24,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster10.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster10);

                MapMonster mapMonster11 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 15,
                    MapX = 22,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster11.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster11);

                MapMonster mapMonster12 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 13,
                    MapX = 30,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster12.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster12);

                MapMonster mapMonster13 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 16,
                    MapX = 30,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster13.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster13);

                MapMonster mapMonster14 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 23,
                    MapX = 43,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster14.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster14);

                MapMonster mapMonster15 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 30,
                    MapX = 43,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster15.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster15);

                MapMonster mapMonster16 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 38,
                    MapX = 39,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster16.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster16);

                MapMonster mapMonster17 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 45,
                    MapX = 30,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster17.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster17);

                MapMonster mapMonster18 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 14,
                    MapX = 21,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster18.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster18);

                MapMonster mapMonster19 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 34,
                    MapX = 15,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster19.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster19);

                MapMonster mapMonster20 = new MapMonster
                {
                    MonsterVNum = 620,
                    MapY = 26,
                    MapX = 13,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = true
                };
                mapMonster20.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(mapMonster20);

                MapMonster BossMonster = new MapMonster
                {
                    MonsterVNum = 624,
                    MapY = 27,
                    MapX = 27,
                    MapId = Berios.BeriosMapInstance.Map.MapId,
                    Position = 7,
                    IsMoving = true,
                    MapMonsterId = Berios.BeriosMapInstance.GetNextMonsterId(),
                    ShouldRespawn = false
                };
                BossMonster.Initialize(Berios.BeriosMapInstance);
                Berios.BeriosMapInstance.AddMonster(BossMonster);

                MapMonster Boss = Berios.BeriosMapInstance.Monsters.Find(s => s.Monster.NpcMonsterVNum == 624);

                if (Boss != null)
                {
                    Boss.BattleEntity.OnDeathEvents = onDeathEvents;
                    Boss.IsBoss = true;
                }

                ServerManager.Shout(Language.Instance.GetMessageFromKey("BERIOS_OPEN"), true);

                RefreshRaid(Berios.RemainingTime);

                ServerManager.Instance.BeriosBossStart = DateTime.Now;

                while (Berios.RemainingTime > 0)
                {
                    Berios.RemainingTime -= interval;
                    Thread.Sleep(interval * 1000);
                    RefreshRaid(Berios.RemainingTime);
                }

                EndRaid();
            }
            private void EndRaid()
            {
                ServerManager.Shout(Language.Instance.GetMessageFromKey("BERIOS_END"), true);

                foreach (Portal p in Berios.BeriosStartMap.Portals.Where(s => s.DestinationMapInstanceId == Berios.BeriosStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Berios.BeriosStartMap.Broadcast(p.GenerateGp());
                    Berios.BeriosStartMap.Portals.Remove(p);
                }
                foreach (ClientSession sess in Berios.BeriosStartMap.Sessions.ToList())
                {
                    ServerManager.Instance.ChangeMapInstance(sess.Character.CharacterId, Berios.BeriosStartMap.MapInstanceId, sess.Character.MapX, sess.Character.MapY);
                    Thread.Sleep(100);
                }
                EventHelper.Instance.RunEventAsync(new EventContainer(Berios.BeriosStartMap, EventActionType.DISPOSEMAP, null));
                Berios.IsRunning = false;
                Berios.AngelDamage = 0;
                Berios.DemonDamage = 0;
                ServerManager.Instance.StartedEvents.Remove(EventType.BeriosBoss);
            }

            private void LockRaid()
            {
                foreach (Portal p in Berios.BeriosStartMap.Portals.Where(s => s.DestinationMapInstanceId == Berios.BeriosStartMap.MapInstanceId).ToList())
                {
                    p.IsDisabled = true;
                    Berios.BeriosStartMap.Broadcast(p.GenerateGp());
                    p.IsDisabled = false;
                    p.Type = (byte)PortalType.Closed;
                    Berios.BeriosStartMap.Broadcast(p.GenerateGp());
                }
                ServerManager.Shout(Language.Instance.GetMessageFromKey("BERIOS_LOCKED"), true);
                Berios.IsLocked = true;
            }

            private void RefreshRaid(int remaining)
            {
                int maxHp = ServerManager.GetNpcMonster(629).MaxHP;
                Berios.BeriosMapInstance.Broadcast(UserInterfaceHelper.GenerateCHDM(maxHp, Berios.AngelDamage, Berios.DemonDamage, Berios.RemainingTime));

                if (((maxHp / 10) * 8 < Berios.AngelDamage + Berios.DemonDamage) && !Berios.IsLocked)
                {
                    LockRaid();
                }
            }
        }
    }
}
#endregion
