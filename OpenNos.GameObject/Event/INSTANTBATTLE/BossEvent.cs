﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.Domain;
using OpenNos.GameObject.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using OpenNos.GameObject.Networking;

namespace OpenNos.GameObject.Event
{
    public static class BossEvent
    {
  
        #region Methods

        public static void GenerateBossEvent()
        {
            //tell PEOPLE that the IC will start in X
            
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} minutes", 5), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} minutes", 5), 1));
            Thread.Sleep(4 * 60 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} minutes", 1), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} minutes", 1), 1));
            Thread.Sleep(30 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} seconds", 30), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} seconds", 30), 1));
            Thread.Sleep(20 * 1000);
            
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} seconds", 10), 0));
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("The boss event will start in {0} seconds", 10), 1));
            Thread.Sleep(10 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg("THE BOSS EVENT HAS STARTED", 1));
            //send to everyone the request ic packet: IC icon appears, you click on it and see the string (battlequestion)
            ServerManager.Instance.Sessions.Where(s => s.Character?.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance).ToList().ForEach(s => s.SendPacket($"qnaml 3 #guri^506 TRY THIS EVENT"));
            ServerManager.Instance.EventInWaiting = true;
            //wait before teleporting everyone
            Thread.Sleep(30 * 1000);
            ServerManager.Instance.Broadcast(UserInterfaceHelper.GenerateMsg("THE BOSS EVENT HAS STARTED", 1));
            ServerManager.Instance.Sessions.Where(s => s.Character?.IsWaitingForEvent == false).ToList().ForEach(s => s.SendPacket("esf"));
            ServerManager.Instance.EventInWaiting = false;
            IEnumerable<ClientSession> sessions = ServerManager.Instance.Sessions.Where(s => s.Character?.IsWaitingForEvent == true && s.Character.MapInstance.MapInstanceType == MapInstanceType.BaseMapInstance);
            List<Tuple<MapInstance, byte>> maps = new List<Tuple<MapInstance, byte>>(); 
            MapInstance map = null;
            int i = -1;
            int level = 0;
                                                // ! FIRST MAP !
            short[] bufferMaps = { 2502 };//, 2542,  2004}; //maps 

            Random rnd = new Random();
            int casualC = 0;//rnd.Next(0, 3); // Next(min, max - 1) # Next(0,3) -> from 0 to 2
            //int[] portalCoordinatesX = {69, 42, 35}; //x  [NOT USED YET]
            //int[] portalCoordinatesY = {39, 48, 32}; //y
            byte instancelevel = 80;
            //put every character (person) into the ic at their level (it is a list)
            foreach (ClientSession s in sessions.OrderBy(s => s.Character?.Level))
            {
                i++;
                
                //if the players reach 50, create a new map and insert the next ones [SIANA LIKED THIS METHOD]
                if (i % 50 == 0)
                {
                    map = ServerManager.GenerateMapInstance(bufferMaps[casualC], MapInstanceType.NormalInstance, new InstanceBag());
                    maps.Add(new Tuple<MapInstance, byte>(map, instancelevel));
                }
                if (map != null)
                {
                    ServerManager.Instance.TeleportOnRandomPlaceInMap(s, map.MapInstanceId);
                }

                level = s.Character.Level;
            }
            //insert only the people that are registered
            ServerManager.Instance.Sessions.Where(s => s.Character != null).ToList().ForEach(s => s.Character.IsWaitingForEvent = false);
            ServerManager.Instance.StartedEvents.Remove(EventType.BOSSEVENT);
            foreach (Tuple<MapInstance, byte> mapinstance in maps)
            {
                RandomMonsterEVTask task = new RandomMonsterEVTask();
                Observable.Timer(TimeSpan.FromMinutes(0)).Subscribe(X => RandomMonsterEVTask.Run(mapinstance, 0));
            }
        }

        #endregion

        #region Classes

        public class RandomMonsterEVTask
        {
            private static int maxRound = 8 - 1;
            #region Methods

            public static void Run(Tuple<MapInstance, byte> mapinstance, int Nround)
            {
                short[] battleMaps = {2100, 4680, 4683, 2103, 2542, 4503, 2555 }; //maps BUFFER
                //create the var maxGold so when giving the rewards, there won't be any problems in overflowing the server datas
                long maxGold = ServerManager.Instance.Configuration.MaxGold;
                //10 seconds before checking/starting
                Thread.Sleep(10 * 1000);
                //check if on the map there are 3 people at least (if the objects are not empty)
                
                if (!mapinstance.Item1.Sessions.Skip(1 - 1).Any()) //because it does +1, that's why we subtract 1
                {
                    //do for all registered characters
                    mapinstance.Item1.Sessions.Where(s => s.Character != null).ToList().ForEach(s =>
                    {
                        //if the character have this buffs (hiding buffs), then remove them

                        s.Character.RemoveBuffByBCardTypeSubType(new List<KeyValuePair<byte, byte>>()
                        {
                            new KeyValuePair<byte, byte>((byte)BCardType.CardType.SpecialActions, (byte)AdditionalTypes.SpecialActions.Hide),
                            new KeyValuePair<byte, byte>((byte)BCardType.CardType.FalconSkill, (byte)AdditionalTypes.FalconSkill.Hide),
                            new KeyValuePair<byte, byte>((byte)BCardType.CardType.FalconSkill, (byte)AdditionalTypes.FalconSkill.Ambush)
                        });

                        //teleport the character to the map
                        ServerManager.Instance.ChangeMap(s.Character.CharacterId, s.Character.MapId, s.Character.MapX, s.Character.MapY);
                    });
                }
                mapinstance.Item1.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("GET READT TO HAVE FUN ! ", Nround + 1), 0));

                getHistoryMessage(mapinstance.Item1.Map, mapinstance.Item2, Nround, mapinstance);

                Observable.Timer(TimeSpan.FromSeconds(15)).Subscribe(X =>
                {//we've put 180 seconds (delay 1 sec each cycle) because the ic is 15 min and we start this at min 12 (180 sec = 3 min)
                    for (int d = 0; d < 900; d++)
                    {
                        //if every monster is dead in ic
                        if (!mapinstance.Item1.Monsters.Any(s => s.CurrentHp > 0))
                        {
                            //drop the items
                            int Nrund = Nround;
                            EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(3), new EventContainer(mapinstance.Item1, EventActionType.DROPITEMS, getInstantBattleDrop(mapinstance.Item1.Map, mapinstance.Item2, Nround)));
                            if (Nround != maxRound)
                            {
                                mapinstance.Item1.Broadcast(UserInterfaceHelper.GenerateMsg(string.Format("YOU'VE CLEARED FLOOR {0}", Nround+1), 0));
                            }
                            else
                            {
                                Nrund--;
                            }
                            //create a portal into the event map
                            //EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(0), new EventContainer(mapinstance.Item1, EventActionType.SPAWNPORTAL, new Portal { SourceX = 20, SourceY = 45, DestinationMapId = 2502 }));
                            MapInstance map = null;

                            map = ServerManager.GenerateMapInstance(battleMaps[Nrund], MapInstanceType.NormalInstance, new InstanceBag());
                            List<Tuple<MapInstance, byte>> maps = new List<Tuple<MapInstance, byte>>();
                            maps.Add(new Tuple<MapInstance, byte>(map, 2));


                            //for everyone in the ic, give the following rewards
                            foreach (ClientSession cli in mapinstance.Item1.Sessions.Where(s => s.Character != null).ToList())
                            {
                                cli.Character.GenerateFamilyXp(cli.Character.Level * 4);
                                cli.Character.GetReputation(cli.Character.Level * 50);
                                cli.Character.Gold += cli.Character.Level * 1000;
                                cli.Character.Gold = cli.Character.Gold > maxGold ? maxGold : cli.Character.Gold;
                                cli.Character.SpAdditionPoint += cli.Character.Level * 100;

                                if (cli.Character.SpAdditionPoint > 1000000)
                                {
                                    cli.Character.SpAdditionPoint = 1000000;
                                }

                                cli.SendPacket(cli.Character.GenerateSpPoint());
                                cli.SendPacket(cli.Character.GenerateGold());
                                cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_MONEY"), cli.Character.Level * 1000), 10));
                                cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_REPUT"), cli.Character.Level * 50), 10));
                                if (cli.Character.Family != null)
                                {
                                    cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_FXP"), cli.Character.Level * 4), 10));
                                }
                                cli.SendPacket(cli.Character.GenerateSay(string.Format(Language.Instance.GetMessageFromKey("WIN_SP_POINT"), cli.Character.Level * 100), 10));
                                //2100
                                if (Nround != maxRound)
                                {
                                    Observable.Timer(TimeSpan.FromSeconds(20)).Subscribe(Y => ServerManager.Instance.TeleportOnRandomPlaceInMap(cli, map.MapInstanceId));
                                }

                            }
                            foreach (Tuple<MapInstance, byte> mapinstance2 in maps)
                            {
                                if (Nround != maxRound)
                                {
                                    Observable.Timer(TimeSpan.FromSeconds(5)).Subscribe(Y => RandomMonsterEVTask.Run(mapinstance2, Nround+1));
                                }
                                else  // BATTLE WONz
                                {
                                    mapinstance.Item1.Broadcast(UserInterfaceHelper.GenerateMsg("CONGRATULATIONS, YOU'VE CLEARED THE BOSS BATTLE !!", 0));
                                    EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(0), new EventContainer(mapinstance.Item1, EventActionType.SPAWNPORTAL, new Portal { SourceX = 21, SourceY = 37, DestinationMapId = 2502 }));

                                }
                            }

                            break;
                        }
                        Thread.Sleep(1000);
                    }
                });

                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(2), new EventContainer(mapinstance.Item1, EventActionType.DISPOSEMAP, null));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(1), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} minutes", 12), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} minutes", 10), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(10), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} minutes", 5), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(11), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} minutes", 4), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(12), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} minutes", 3), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(13), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} minutes", 2), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(14), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} minutes", 1), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(14.5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} seconds", 30), 0)));
                EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(14.5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg(string.Format("The boss event will finish in {0} seconds", 30), 0)));
                //EventHelper.Instance.ScheduleEvent(TimeSpan.FromMinutes(0), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("THE BOSS IS COMING !!! GET READY !", 0)));


                         //getHistoryMessage(mapinstance.Item1.Map, mapinstance.Item2, Nround, mapinstance);
                
                
                //EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("THE BOSS IS HERE !", 0)));

                //EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10), new EventContainer(mapinstance.Item1, EventActionType.SPAWNMONSTERS, getInstantBattleMonster(mapinstance.Item1.Map, mapinstance.Item2, Nround)));
                //spawn the boss monster
                //EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10 + (0 * 160)), new EventContainer(mapinstance.Item1, EventActionType.SPAWNMONSTERS, getInstantBattleMonster(mapinstance.Item1.Map, mapinstance.Item2, 4)));
            }


            //the dropping (ez read)
            private static IEnumerable<Tuple<short, int, short, short>> generateDrop(Map map, short vnum, int amountofdrop, int amount)
            {
                List<Tuple<short, int, short, short>> dropParameters = new List<Tuple<short, int, short, short>>();
                for (int i = 0; i < amountofdrop; i++)
                {
                    MapCell cell = map.GetRandomPosition();
                    dropParameters.Add(new Tuple<short, int, short, short>(vnum, amount, cell.X, cell.Y));
                }
                return dropParameters;
            }

            //drop loot by waves
            private static List<Tuple<short, int, short, short>> getInstantBattleDrop(Map map, short instantbattletype, int wave)
            {
                List<Tuple<short, int, short, short>> dropParameters = new List<Tuple<short, int, short, short>>();
                //for loots
                switch (wave)
                {
                    case 0:
                        dropParameters.AddRange(generateDrop(map, 1011, 150, 5));
                        dropParameters.AddRange(generateDrop(map, 2282, 150, 5));
                        break;

                    case 1:
                        dropParameters.AddRange(generateDrop(map, 1046, 50, 200000));
                        dropParameters.AddRange(generateDrop(map, 10016, 150, 5));
                        dropParameters.AddRange(generateDrop(map, 10050, 150, 5));
                        break;

                    case 2:
                        dropParameters.AddRange(generateDrop(map, 1046, 15, 15000));
                        dropParameters.AddRange(generateDrop(map, 1011, 20, 5));
                        dropParameters.AddRange(generateDrop(map, 1246, 15, 1));
                        dropParameters.AddRange(generateDrop(map, 1247, 15, 1));
                        break;

                    case 3:
                        dropParameters.AddRange(generateDrop(map, 1046, 30, 20000));
                        dropParameters.AddRange(generateDrop(map, 1011, 30, 5));
                        dropParameters.AddRange(generateDrop(map, 1030, 30, 1));
                        dropParameters.AddRange(generateDrop(map, 2282, 12, 3));
                        break;
                    case 4:
                        dropParameters.AddRange(generateDrop(map, 1046, 30, 20000));
                        dropParameters.AddRange(generateDrop(map, 1011, 30, 5));
                        dropParameters.AddRange(generateDrop(map, 1030, 30, 1));
                        dropParameters.AddRange(generateDrop(map, 2282, 12, 3));
                        break;
                    case 5:
                        dropParameters.AddRange(generateDrop(map, 1046, 30, 20000));
                        dropParameters.AddRange(generateDrop(map, 1011, 30, 5));
                        dropParameters.AddRange(generateDrop(map, 1030, 30, 1));
                        dropParameters.AddRange(generateDrop(map, 2282, 12, 3));
                        break;
                    case 6:
                        dropParameters.AddRange(generateDrop(map, 1046, 30, 20000));
                        dropParameters.AddRange(generateDrop(map, 1011, 30, 5));
                        dropParameters.AddRange(generateDrop(map, 1030, 30, 1));
                        dropParameters.AddRange(generateDrop(map, 2282, 12, 3));
                        break;
                    case 7:
                        dropParameters.AddRange(generateDrop(map, 1046, 30, 20000));
                        dropParameters.AddRange(generateDrop(map, 1011, 30, 5));
                        dropParameters.AddRange(generateDrop(map, 1030, 30, 1));
                        dropParameters.AddRange(generateDrop(map, 2282, 12, 3));
                        break;
                    case 8:
                        dropParameters.AddRange(generateDrop(map, 1046, 30, 20000));
                        dropParameters.AddRange(generateDrop(map, 1011, 30, 5));
                        dropParameters.AddRange(generateDrop(map, 1030, 30, 1));
                        dropParameters.AddRange(generateDrop(map, 2282, 12, 3));
                        break;
                }
                return dropParameters;
            }
        
     
            //summon monsters by waves
            private static List<MonsterToSummon> getInstantBattleMonster(Map map, short instantbattletype, int wave)
            {
                List<MonsterToSummon> SummonParameters = new List<MonsterToSummon>();
                switch (wave)
                {
                    case 0: // castra (285) hellknight (724)
                        SummonParameters.AddRange(map.GenerateMonsters(285, 1, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(724, 1, true, new List<EventContainer>()));
                        break;

                    case 1: //whiteYerti (2651) vampireMob x2 (464) swordspell x1 (2594) swordspell2 x1 (2595) meteorSmall x5(2591) largeMeteor x3 (2592)
                        SummonParameters.AddRange(map.GenerateMonsters(464, 2, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2651, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2595, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2591, 5, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2592, 3, true, new List<EventContainer>()));
                        break;

                    case 2: //amora x10 (2641) peng x8 (2205) monkey x6 (2207)
                        SummonParameters.AddRange(map.GenerateMonsters(2641, 10, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2205, 8, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2207, 6, true, new List<EventContainer>()));
                        break;

                    case 3: // glacerus (2049)
                        SummonParameters.AddRange(map.GenerateMonsters(2049, 1, true, new List<EventContainer>()));
                        break;

                    case 4:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(11015, 1, true, new List<EventContainer>()));
                        break;
                    case 5:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 6:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 7:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 8:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 100:
                        SummonParameters.AddRange(map.GenerateMonsters(2205, 8, true, new List<EventContainer>()));
                        break;
                }
                return SummonParameters;
            }

            //show history messages
            private static List<MonsterToSummon> getHistoryMessage(Map map, short instantbattletype, int wave, Tuple<MapInstance, byte> mapinstance)
            {
                List<MonsterToSummon> SummonParameters = new List<MonsterToSummon>();
                switch (wave)
                {
                    case 0: // castra (285) hellknight (724)
                        //mapinstance.Item1.Broadcast(UserInterfaceHelper.GenerateMsg("THE STORY OF OUR HEROES BEGINS", 0));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(2), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("THE STORY OF OUR HEROES BEGINS", 0)));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("GET READY TO TASTE THEIR POWER", 0)));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(8), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("AND BECOME ONE OF THEM", 0)));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10), new EventContainer(mapinstance.Item1, EventActionType.SPAWNMONSTERS, getInstantBattleMonster(mapinstance.Item1.Map, mapinstance.Item2, wave)));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(11), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("GOOD LUCK INTO KILLING THE BOSS !", 0)));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(14), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("HOPEFULY IT WORKS !", 0)));
                        break;

                    case 1: //whiteYerti (2651) vampireMob x2 (464) swordspell x1 (2594) swordspell2 x1 (2595) meteorSmall x5(2591) largeMeteor x3 (2592)
                        SummonParameters.AddRange(map.GenerateMonsters(464, 2, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2651, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2595, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2591, 5, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2592, 3, true, new List<EventContainer>()));
                        break;

                    case 2: //amora x10 (2641) peng x8 (2205) monkey x6 (2207)
                        SummonParameters.AddRange(map.GenerateMonsters(2641, 10, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2205, 8, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2207, 6, true, new List<EventContainer>()));
                        break;

                    case 3: // glacerus (2049)
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(2), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("ALLAHU AKBAR", 0)));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("HE SUMMONS YOU", 0)));
                        EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10), new EventContainer(mapinstance.Item1, EventActionType.SPAWNMONSTERS, getInstantBattleMonster(mapinstance.Item1.Map, mapinstance.Item2, 100)));

                        for (int d = 0; d < 900; d++)
                        {
                            //if every monster is dead in ic
                            if (!mapinstance.Item1.Monsters.Any(s => s.CurrentHp > 0))
                            {
                                EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(2), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("BEFORE HE DIES", 0)));
                                EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(5), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("HE SUMMONS SUPPORTERS", 0)));
                                EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(10), new EventContainer(mapinstance.Item1, EventActionType.SPAWNMONSTERS, getInstantBattleMonster(mapinstance.Item1.Map, mapinstance.Item2, wave)));
                                EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(11), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("THEY ARE STRONG", 0)));
                                EventHelper.Instance.ScheduleEvent(TimeSpan.FromSeconds(14), new EventContainer(mapinstance.Item1, EventActionType.SENDPACKET, UserInterfaceHelper.GenerateMsg("HOPEFULY IT WORKS !", 0)));

                            }
                            Thread.Sleep(1000);
                            break;
                        }
                        break;

                    case 4:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(11015, 1, true, new List<EventContainer>()));
                        break;
                    case 5:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 6:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 7:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 8:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                }
                return SummonParameters;
            }

            //summon monsters by waves
            /*
            private static List<MonsterToSummon> getInstantSubWaveMonster(Map map, short instantbattletype, int wave, int subwave)
            {
                List<MonsterToSummon> SummonParameters = new List<MonsterToSummon>();
                switch (wave)
                {
                    case 0: // castra (285) hellknight (724)
                        switch (subwave)
                        {
                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(556, 1, true, new List<EventContainer>()));
                                break;
                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(2510, 1, true, new List<EventContainer>()));
                                break;
                        }
                        break;

                    case 1: //whiteYerti (2651) vampireMob x2 (464) swordspell x1 (2594) swordspell2 x1 (2595) meteorSmall x5(2591) largeMeteor x3 (2592)
                        SummonParameters.AddRange(map.GenerateMonsters(464, 2, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2651, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2595, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2591, 5, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(2592, 3, true, new List<EventContainer>()));
                        break;

                    case 2: //amora x10 (2641) peng x8 (2205) monkey x6 (2207)
                        SummonParameters.AddRange(map.GenerateMonsters(2641, 10, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2205, 8, true, new List<EventContainer>()));
                        SummonParameters.AddRange(map.GenerateMonsters(2207, 6, true, new List<EventContainer>()));
                        break;

                    case 3: // mukraju (556)  changemob(2510)
                        switch (subwave)
                        {
                            case 1:
                                SummonParameters.AddRange(map.GenerateMonsters(556, 1, true, new List<EventContainer>()));
                                break;
                            case 2:
                                SummonParameters.AddRange(map.GenerateMonsters(2510, 1, true, new List<EventContainer>()));
                                break;
                        }
                        break;

                    case 4:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        //SummonParameters.AddRange(map.GenerateMonsters(11015, 1, true, new List<EventContainer>()));
                        break;
                    case 5:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 6:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 7:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                    case 8:
                        SummonParameters.AddRange(map.GenerateMonsters(2530, 1, true, new List<EventContainer>()));
                        break;
                }
                return SummonParameters;
            } */
            #endregion
        }
        #endregion
    }
}